﻿using ETModel;

namespace ETHotfix
{
    /// <summary>
    /// Actor消息发送助手，游戏服向Gate服发送消息，由Gate直接转发给Client
    /// </summary>
    public static class ActorHelp
    {
        public static void SendeActor(long actorId, IActorMessage iActorMessage)
        {
            if (actorId == 0)
            {
                return;
            }
            ActorMessageSender actorSender = Game.Scene.GetComponent<ActorMessageSenderComponent>().Get(actorId);
            actorSender.Send(iActorMessage);
        }
    }
}
