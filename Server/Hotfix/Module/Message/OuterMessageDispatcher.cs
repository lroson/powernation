﻿using ETModel;

namespace ETHotfix
{
    public class OuterMessageDispatcher : IMessageDispatcher
    {
        public void Dispatch(Session session, ushort opcode, object message)
        {
            DispatchAsync(session, opcode, message).Coroutine();
        }

        public async ETVoid DispatchAsync(Session session, ushort opcode, object message)
        {
            SessionHeartbeatComponent sessionHeartbeatComponent = session.GetComponent<SessionHeartbeatComponent>();
            SessionUserComponent sessionUserComponent = session.GetComponent<SessionUserComponent>();
            if (sessionHeartbeatComponent == null)
            {
                session.Dispose();//心跳组件 没有 直接销毁
                return;
            }
            sessionHeartbeatComponent.UpReceiveMessageDistance = 0;//重置上次收到消息的时间
                                                                   //如果没有挂载SessionUserComponent组件 需要判断一下是不是登陆消息
            if (sessionUserComponent == null)
            {
                //接收这三个消息时候是没有挂载sessionUserComponent的
                if (message.GetType() != typeof(C2G_GateLogin) && message.GetType() != typeof(C2R_CommonLogin) && message.GetType() != typeof(C2M_Reload))
                {
                    session.Dispose();//发其他 消息 却没有 SessionUserComponent 组件 绝对不走正常
                    return;
                }
            }
            //如果收到 一秒收到的消息 大于规定的消息 就认定是DOSS攻击 直接销毁
            if (++sessionHeartbeatComponent.SecondTotalMessageNum >=
                SessionHeartbeatComponent.DestroySeesiontSecondTotalNum)
            {
                //断开连接
                sessionHeartbeatComponent.DisposeSession();
                //直接封号
                //  UserHelp.StopSealOrRelieve(sessionUserComponent.UserId,true,"DOSS攻击封号"); //不要封号容易误封
                return;
            }


            // 根据消息接口判断是不是Actor消息，不同的接口做不同的处理
            switch (message)
            {
                case IActorLocationRequest actorLocationRequest: // gate session收到actor rpc消息，先向actor 发送rpc请求，再将请求结果返回客户端
                    {
                        long userId = session.GetComponent<SessionUserComponent>().UserId;
                        ActorLocationSender actorLocationSender = Game.Scene.GetComponent<ActorLocationSenderComponent>().Get(userId);

                        int rpcId = actorLocationRequest.RpcId; // 这里要保存客户端的rpcId
                        long instanceId = session.InstanceId;
                        IResponse response = await actorLocationSender.Call(actorLocationRequest);
                        response.RpcId = rpcId;

                        // session可能已经断开了，所以这里需要判断
                        if (session.InstanceId == instanceId)
                        {
                            session.Reply(response);
                        }

                        break;
                    }
                case IActorLocationMessage actorLocationMessage:
                    {
                        long userId = session.GetComponent<SessionUserComponent>().UserId;
                        ActorLocationSender actorLocationSender = Game.Scene.GetComponent<ActorLocationSenderComponent>().Get(userId);
                        actorLocationSender.Send(actorLocationMessage).Coroutine();
                        break;
                    }
                case IActorRequest actorRequest:  // 分发IActorRequest消息，目前没有用到，需要的自己添加
                    {
                        break;
                    }
                case IActorMessage actorMessage:  // 分发IActorMessage消息，目前没有用到，需要的自己添加
                    {
                        break;
                    }
                case C2G_Heartbeat c2GHeartbeat:
                    break;
                //TODO IUserRequest(IUserActorRequest\IAdministratorRequest)没有处理
                //case IUserRequest iUserRequest:
                //{
                //    if (iUserRequest is IUserActorRequest userActorRequest)
                //    {
                //        userActorRequest.SessionActorId = session.Id;
                //        userActorRequest.User = sessionUserComponent.user;
                //    }
                //    if (iUserRequest is IAdministratorRequest administratorRequest)
                //    {
                //        if (!AdministratorHelp.VerifyAdministrator(administratorRequest))
                //        {
                //            //如果账号密码错误 直接返回 不如直接封号
                //            return;
                //        }
                //    }
                //    iUserRequest.UserId = sessionUserComponent.UserId;
                //    AppType appType = (AppType)(1 << ((opcode - 10000) / 1000));
                //    Session serverSession = Game.Scene.GetComponent<NetInnerSessionComponent>().Get(appType);
                //    int rpcId = iUserRequest.RpcId; // 这里要保存客户端的rpcId
                //    IResponse response = await serverSession.Call(iUserRequest);
                //    response.RpcId = rpcId;
                //    session.Reply(response);
                //    return;
                //}
                default:
                    {
                        // 非Actor消息
                        Game.Scene.GetComponent<MessageDispatcherComponent>().Handle(session, new MessageInfo(opcode, message));
                        break;
                    }
            }
        }
    }
}
