﻿using ETHotfix;
using System.Collections.Generic;

namespace ETModel
{
    public static class UserComponentGetGoodsSystem
    {
        public static async ETTask<User> UserGetGoods(this UserComponent self, long userId, List<GetGoodsOne> goodsOnes,int goodsChangeType, bool isShowHintPanel)
        {
            bool userOnline = true;
            User user = self.GetUser(userId);
            if (user == null)
            {
                user = await UserHelp.QueryUserInfo(userId);
                userOnline = false;
            }
            if (user == null)
            {
                Log.Error("要增加物品的玩家更本不存在UserId:" + userId);
                return null;
            }
            
            user.UpdataGoods(goodsOnes);//更新物品数量
            self.SaveGoodsDealRecord(user, goodsOnes, goodsChangeType); //存储物品 变化记录 只会存储钻石的
            await self.SaveUserDB(user);
            if (userOnline)
            {
                Actor_UserGetGoods actorUserGetGoods = new Actor_UserGetGoods();

                foreach (var goods in goodsOnes)
                {
                    switch (goods.GoodsId)
                    {
                        case GoodsId.Coins:
                            goods.NowAmount = user.Coins;
                            break;
                        case GoodsId.Gold:
                            goods.NowAmount = user.Gold;
                            break;
                    }
                }
                actorUserGetGoods.GetGoodsList.AddRange(goodsOnes.ToArray());
                actorUserGetGoods.IsShowHintPanel = isShowHintPanel;
                //向User发送Actor
                user.SendeSessionClientActor(actorUserGetGoods);
            }
            return user;
        }

        //存储物品 变化记录 只会存储钻石的
        public static async void SaveGoodsDealRecord(this UserComponent self, User user, List<GetGoodsOne> goodsOnes,int changeType)
        {
            for (int i = 0; i < goodsOnes.Count; i++)
            {
                if (goodsOnes[i].GoodsId != GoodsId.Gold)
                {
                    continue;
                }
                GoodsDealRecord goodsDealRecord = ComponentFactory.Create<GoodsDealRecord>();
                goodsDealRecord.UserId = user.UserId;
                goodsDealRecord.Amount = goodsOnes[i].GetAmount;
                goodsDealRecord.Type = changeType;
                goodsDealRecord.Time = TimeTool.GetCurrenTimeStamp();
                goodsDealRecord.FinishNowAmount = (int)user.Gold;
                await self.dbProxyComponent.Save(goodsDealRecord);
            }
           
        }
    }
}
