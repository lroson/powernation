﻿using ETModel;

namespace ETHotfix
{
    /// <summary>
    /// 用户服用户上线、下线、注册、登录发起组件
    /// </summary>
    public static class UserComponentSystem
    {
        public static User GetUser(this UserComponent userComponent, long userId)
        {
            User user;
            if (!userComponent.OnlineUserDic.TryGetValue(userId, out user))
            {
                //Log.Error($"玩家{userId}不存在或不在游戏中");
            }
            return user;
        }

        //玩家上线事件
        public static async ETTask<User> UserOnLine(this UserComponent userComponent, long userId, long sessionActorId)
        {
            //有用户已经在线，踢号
            User user = userComponent.GetUser(userId);
            if (user != null)
            {
                user.KickAccount();
                user.GetComponent<UserGateActorIdComponent>().ActorId = sessionActorId;
                return user;//如果User本就在线 发送强制下线消息 就改变一下actorid 
            }
            user = await UserHelp.QueryUserInfo(userId);
            if (user != null)
            {
                user.AddComponent<UserGateActorIdComponent>().ActorId = sessionActorId;
                userComponent.OnlineUserDic[user.UserId] = user;
                user.IsOnLine = true;
            }
            return user;
        }
        //玩家下线事件
        public static async void UserOffline(this UserComponent userComponent, long userId)
        {
            if (userComponent.OnlineUserDic.ContainsKey(userId))
            {
                userComponent.OnlineUserDic[userId].IsOnLine = false;
                userComponent.OnlineUserDic[userId].RemoveComponent<UserGateActorIdComponent>();
                await userComponent.dbProxyComponent.Save(userComponent.OnlineUserDic[userId]);
                userComponent.OnlineUserDic.Remove(userId);

            }
        }

        //用户登陆
        public static async ETTask<AccountInfo> LoginOrRegister(this UserComponent userComponent, string dataStr, int loginType)
        {
            AccountInfo accountInfo = null;
            switch (loginType)
            {
                case LoginType.Editor:
                case LoginType.Tourist:
                    accountInfo = await userComponent.EditorLogin(dataStr);
                    break;
                case LoginType.WeChat:
                    accountInfo = await userComponent.WeChatLogin(dataStr);
                    break;
                case LoginType.Voucher://检验凭证
                    accountInfo = await userComponent.VoucherLogin(dataStr);
                    break;
                default:
                    Log.Error("不存在的登陆方式:" + loginType);
                    break;
            }
            //更新最后登录时间
            if (accountInfo != null)
            {
                accountInfo.LastLoginTime = TimeTool.GetCurrenTimeStamp();
                await userComponent.dbProxyComponent.Save(accountInfo);
            }
            return accountInfo;
        }

        public static async ETTask<User> SaveUserDB(this UserComponent userComponent, User user)
        {
            await userComponent.dbProxyComponent.Save(user);
            return user;
        }
    }
}
