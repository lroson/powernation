﻿using ETModel;
using System;
using System.Collections.Generic;

namespace ETHotfix
{
    /// <summary>
    /// 用户服账号登录、注册，及用户注册组件
    /// </summary>
    public static class UserComponentLoginSystem
    {
        //编辑状态下登陆
        public static async ETTask<AccountInfo> EditorLogin(this UserComponent userComponent, string account)
        {
            List<AccountInfo> accountInfos = await userComponent.dbProxyComponent.Query<AccountInfo>(AccountInfo => AccountInfo.Account == account);
            if (accountInfos.Count == 0)
            {
                AccountInfo accountInfo = await UserFactory.EditRegisterCreatUser(account);
                return accountInfo;
            }
            return accountInfos[0];
        }

        //微信登陆
        public static async ETTask<AccountInfo> WeChatLogin(this UserComponent userComponent, string accessTokenAndOpenid)
        {
            WeChatJsonData weChatJsonData = WeChatJsonAnalysis.HttpGetUserInfoJson(accessTokenAndOpenid);
            if (weChatJsonData == null)
            {
                return null;
            }
            List<AccountInfo> accountInfos = await userComponent.dbProxyComponent.Query<AccountInfo>(AccountInfo => AccountInfo.Account == weChatJsonData.unionid);
            if (accountInfos.Count == 0)
            {
                AccountInfo accountInfo = await UserFactory.WeChatRegisterCreatUser(weChatJsonData);
                return accountInfo;
            }

            var result = accountInfos[0];
            result.Password = TimeTool.GetCurrenTimeStamp().ToString();
            await userComponent.dbProxyComponent.Save(result);
            return result;
        }

        //凭证登陆 就是 userId'|'密码
        public static async ETTask<AccountInfo> VoucherLogin(this UserComponent userComponent, string userIdAndPassword)
        {
            string[] userIdPassword = userIdAndPassword.Split('|');
            if (userIdPassword.Length != 2)
            {
                return null;
            }
            try
            {
                long queryUserId = long.Parse(userIdPassword[0]);
                List<AccountInfo> accountInfos = await userComponent.dbProxyComponent.Query<AccountInfo>(AccountInfo =>
                    AccountInfo.UserId == queryUserId && AccountInfo.Password == userIdPassword[1]);
                if (accountInfos.Count > 0)
                {
                    return accountInfos[0];
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
            return null;
        }
    }
}
