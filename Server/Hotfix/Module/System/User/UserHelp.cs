﻿using ETModel;
using Google.Protobuf.Collections;
using System.Collections.Generic;

namespace ETHotfix
{
    /// <summary>
    /// 用户数据库操作助手
    /// </summary>
    public static class UserHelp
    {
        public static async ETTask<User> QueryUserInfo(long userId)
        {
            List<User> users = await Game.Scene.GetComponent<DBProxyComponent>()
                .Query<User>(user => user.UserId == userId);
            if (users.Count > 0)
            {
                return users[0];
            }
            return null;
        }
        public static async ETTask<List<User>> QueryUserInfo(RepeatedField<long> userId)
        {
            List<User> users = await Game.Scene.GetComponent<DBProxyComponent>()
                .Query<User>(user => userId.Contains(user.UserId));
            if (users != null)
            {
                return users;
            }
            return null;
        }
        //封号或者解封
        public static void StopSealOrRelieve(long userId, bool isStopSeal, string explain)
        {
            Session userSession = Game.Scene.GetComponent<NetInnerSessionComponent>().Get(AppType.User);
            //直接封号
            StopSealRecord stopSealRecord = new StopSealRecord();
            stopSealRecord.StopSealUserId = userId;
            stopSealRecord.IsStopSeal = isStopSeal;
            stopSealRecord.Explain = explain;
            userSession.Call(new C2U_SetIsStopSeal() { StopSeal = stopSealRecord });
        }
        //改变用户物品
        public static async ETTask<User> GoodsChange(long userId, long goodId, int amount, int changeType, bool isShowHint = false)
        {
            GetGoodsOne getGoodsOne = ComponentFactory.Create<GetGoodsOne>();
            getGoodsOne.SetGoodsOne(goodId, amount);
            List<GetGoodsOne> getGoodsOnes = new List<GetGoodsOne>() { getGoodsOne };
            User user = await GoodsChange(userId, getGoodsOnes, changeType, isShowHint);
            getGoodsOne.Dispose();
            return user;
        }
        //改变用户物品
        public static async ETTask<User> GoodsChange(long userId, List<GetGoodsOne> getGoodsOnes, int changeType, bool isShowHint = false)
        {
            Session userSession = Game.Scene.GetComponent<NetInnerSessionComponent>().Get(AppType.User);
            U2S_UserGetGoods u2SUserGetGoods = (U2S_UserGetGoods)await userSession.Call(new S2U_UserGetGoods()
            {
                UserId = userId,
                GetGoodsList = getGoodsOnes,
                isShowHintPanel = isShowHint,
                GoodsChangeType = changeType
            });
            return u2SUserGetGoods.user;
        }
    }
}
