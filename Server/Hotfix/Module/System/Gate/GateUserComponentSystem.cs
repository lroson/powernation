﻿using ETModel;


namespace ETHotfix
{
    /// <summary>
    /// Gate服Gate用户管理组件
    /// </summary>
    public static class GateUserComponentSystem
    {
        //获取当前网关连接User中的信息
        public static User GetUser(this GateUserComponent gateUserComponent, long userId)
        {
            User user;
            if (!gateUserComponent.UserDic.TryGetValue(userId, out user))
            {
                //Log.Error($"玩家{userId}不存在或不在游戏中");
            }
            return user;
        }

        //玩家上线事件
        public static async ETTask<User> UserOnLine(this GateUserComponent gateUserComponent, long userId, long sessionActorId)
        {
            User user = await UserHelp.QueryUserInfo(userId);
            if (user == null)
            {
                return null;
            }
            user.IsOnLine = true;//改变在线状态
            //给其他服务器广播玩家上线消息
            gateUserComponent.BroadcastOnAndOffLineMessage(new G2A_UserOnline() { UserId = userId, SessionActorId = sessionActorId });
            //记录玩家信息
            gateUserComponent.UserDic[userId] = user;
            return user;
        }

        //玩家下线事件
        public static void UserOffline(this GateUserComponent gateUserComponent, long userId)
        {
            long gamerSeesionActorId = 0;
            if (gateUserComponent.UserDic.ContainsKey(userId))
            {
                gamerSeesionActorId = gateUserComponent.UserDic[userId].GetUserClientSession().GetComponent<SessionUserComponent>().GamerSessionActorId;
            }
            if (gamerSeesionActorId != 0)
            {
                ActorHelp.SendeActor(gamerSeesionActorId, new Actor_UserOffLine());//TODO 游戏服暂未实现，待改，告诉游戏服 用户下线
            }
            if (gateUserComponent.UserDic.ContainsKey(userId))
            {
                gateUserComponent.UserDic.Remove(userId);
            }
            //给其他服务器广播玩家下线消息
            gateUserComponent.BroadcastOnAndOffLineMessage(new G2A_UserOffline() { UserId = userId });
        }
        //给其他服务器广播用户 上 下线消息
        public static void BroadcastOnAndOffLineMessage(this GateUserComponent self, IMessage iMessage)
        {
            AppType appType = StartConfigComponent.Instance.StartConfig.AppType;
            if (appType == AppType.AllServer)
            {
                self.MatchSession.Send(iMessage);
            }
            else
            {
                self.UserSession.Send(iMessage);
                self.MatchSession.Send(iMessage);
            }
        }
    }
}
