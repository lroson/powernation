﻿//using ETModel;

//namespace ETHotfix.GameGather.Common.Handler.User
//{
//    /// <summary>
//    /// 用户下线
//    /// </summary>
//    [MessageHandler(AppType.Match)]
//    public class G2A_UserOfflineHandlerMh : AMHandler<G2A_UserOffline>
//    {
//        protected override async ETTask Run(Session session, G2A_UserOffline message)
//        {
//            Game.Scene.GetComponent<MatchRoomComponent>().RmoveQueueUser(message.UserId);//直接移除匹配队列
//            Game.Scene.GetComponent<MatchRoomComponent>().PlayerOffline(message.UserId);//通知玩家下线
//            await ETTask.CompletedTask;
//        }
//    }
//}
