﻿using ETModel;
using System;

namespace ETHotfix
{
    [MessageHandler(AppType.Gate)]
    public class C2G_GateLoginHandler : AMRpcHandler<C2G_GateLogin, G2C_GateLogin>
    {
        protected override async ETTask Run(Session session, C2G_GateLogin request, G2C_GateLogin response, Action reply)
        {
            long userId = Game.Scene.GetComponent<GateSessionKeyComponent>().Get(request.Key);
            //添加收取Actor消息组件 所有游戏服可以直接向Client发送消息
            session.AddComponent<MailBoxComponent, string>(MailboxType.GateSession);
            //通知GateUserComponent组件和用户服玩家上线 并获取User实体
            User user = await Game.Scene.GetComponent<GateUserComponent>().UserOnLine(userId, session.Id);
            if (user == null)
            {
                response.Message = "用户信息查询不到";
                reply();
                return;
            }
            //记录客户端session在User中
            user.AddComponent<UserClientSessionComponent>().session = session;
            //给Session组件添加下线监听组件和添加User实体
            session.AddComponent<SessionUserComponent>().user = user;
            //返回客户端User信息和 当前服务器时间
            response.User = user;
            response.ServerTime = TimeTool.GetCurrenTimeStamp();
            reply();
        }
    }
}