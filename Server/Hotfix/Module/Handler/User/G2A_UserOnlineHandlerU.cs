﻿using ETModel;

namespace ETHotfix
{
    /// <summary>
    /// User服用户上线处理
    /// </summary>
    [MessageHandler(AppType.User)]
    public class G2A_UserOnlineHandlerU : AMHandler<G2A_UserOnline>
    {
        protected override async ETTask Run(Session session, G2A_UserOnline message)
        {
            await Game.Scene.GetComponent<UserComponent>().UserOnLine(message.UserId, message.SessionActorId);
        }
    }
}