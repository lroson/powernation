﻿using ETModel;

namespace ETHotfix.GameGather.Common.Handler.User
{
    [MessageHandler(AppType.User)]
    public class G2A_UserOfflineHandlerU : AMHandler<G2A_UserOffline>
    {
        protected override async ETTask Run(Session session, G2A_UserOffline message)
        {
            Game.Scene.GetComponent<UserComponent>().UserOffline(message.UserId);
            await ETTask.CompletedTask;
        }
    }
}
