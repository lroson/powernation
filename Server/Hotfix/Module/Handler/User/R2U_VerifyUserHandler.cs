﻿using System;
using ETModel;

namespace ETHotfix
{
    [MessageHandler(AppType.User)]
    public class R2U_VerifyUserHandler : AMRpcHandler<R2U_VerifyUser, U2R_VerifyUser>
    {
        protected override async ETTask Run(Session session, R2U_VerifyUser request, U2R_VerifyUser response, Action reply)
        {
            AccountInfo accountInfo = (await Game.Scene.GetComponent<UserComponent>().LoginOrRegister(request.DataStr, request.LoginType));
            if (accountInfo == null)
            {
                response.Message = "登陆验证出错,请重新登陆";
                reply();
                return;
            }
            if (accountInfo.IsStopSeal)
            {
                response.Message = $"ID:{accountInfo.UserId}账号已被停封,请联系客服QQ:---------";
                reply();
                return;
            }
            response.UserId = accountInfo.UserId;
            response.Password = accountInfo.Password;
            reply();
        }
    }
}
