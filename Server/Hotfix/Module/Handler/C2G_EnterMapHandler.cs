﻿using System;
using System.Net;
using ETModel;

namespace ETHotfix
{
    /// <summary>
    /// 大厅测试，待正式地图在进入
    /// </summary>
	[MessageHandler(AppType.Gate)]
	public class C2G_EnterMapHandler : AMRpcHandler<C2G_EnterMap, G2C_EnterMap>
	{
        //protected override async ETTask Run(Session session, C2G_EnterMap request, G2C_EnterMap response, Action reply)
        //{
        //	User user = session.GetComponent<SessionUserComponent>().user;
        //	// 在map服务器上创建战斗Unit
        //	IPEndPoint mapAddress = StartConfigComponent.Instance.MapConfigs[0].GetComponent<InnerConfig>().IPEndPoint;
        //	Session mapSession = Game.Scene.GetComponent<NetInnerComponent>().Get(mapAddress);
        //	M2G_CreateUnit createUnit = (M2G_CreateUnit)await mapSession.Call(new G2M_CreateUnit() { PlayerId = user.Id, GateSessionId = session.InstanceId });
        //	user.UserId = createUnit.UnitId;
        //	response.UnitId = createUnit.UnitId;
	    //	reply();
	    //}
	    protected override async ETTask Run(Session session, C2G_EnterMap request, G2C_EnterMap response, Action reply)
	    {
	        response.Message = "测试消息回复！！！！！！！！！！！";
	        reply();
	    }
    }
}