﻿using System;
using System.Collections.Generic;
using ETModel;

namespace ETHotfix
{
    [MessageHandler(AppType.DB)]
    public class DBSortQueryJsonRequestHandler : AMRpcHandler<DBSortQueryJsonRequest, DBQueryJsonResponse>
    {
        private string[] strs = new string[3];
        protected override async ETTask Run(Session session, DBSortQueryJsonRequest request, DBQueryJsonResponse response, Action reply)
        {
            DBComponent dbCacheComponent = Game.Scene.GetComponent<DBComponent>();
            strs[0] = request.CollectionName;
            strs[1] = request.QueryJson;
            strs[2] = request.SortJson;
            List<ComponentWithId> components = await dbCacheComponent.GetJson(strs, request.Count);
            response.Components = components;
            reply();
        }
    }
}