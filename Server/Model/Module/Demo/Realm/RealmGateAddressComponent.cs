﻿using System.Collections.Generic;

namespace ETModel
{
    /// <summary>
    /// 验证服获取Gate地址
    /// </summary>
    public class RealmGateAddressComponent : Component
    {
        public readonly List<StartConfig> GateAddress = new List<StartConfig>();
        private readonly Dictionary<string, StartConfig> AccGateAddressDict = new Dictionary<string, StartConfig>();
        /// <summary>
        /// 获取随机Gate地址，并记录账号所在Gate
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
		public StartConfig GetRandomAddressByAcc(string account)
        {
            int n = RandomHelper.RandomNumber(0, this.GateAddress.Count);
            this.AccGateAddressDict[account] = GateAddress[n];
            return this.GateAddress[n];
        }
        /// <summary>
        /// 取出账号登录所在的Gate服务器地址，并移除相应映射
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
	    public StartConfig GetAddressByAcc(string account)
        {
            if (this.AccGateAddressDict.TryGetValue(account, out StartConfig startConfig))
            {
                this.AccGateAddressDict.Remove(account);
            }
            return startConfig;
        }
        /// <summary>
        /// 获取一个随机Gate地址
        /// </summary>
        /// <returns></returns>
        public StartConfig GetRandomAddress()
        {
            int n = RandomHelper.RandomNumber(0, this.GateAddress.Count);
            return this.GateAddress[n];
        }
    }
}
