﻿using ETHotfix;

namespace ETModel
{
    /// <summary>
    /// Session连接的用户信息和连接Session.InstanceId(ActorId)
    /// </summary>
    public class SessionUserComponent : Component
    {
        public User user { get; set; }//Session所对应的User


        public long UserId
        {
            get { return user.UserId; }
        }
        public long GamerSessionActorId { get; set; }
    }

}
