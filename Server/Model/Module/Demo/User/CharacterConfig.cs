﻿using MongoDB.Bson.Serialization.Attributes;

namespace ETModel
{
    [BsonIgnoreExtraElements]
    public class CharacterConfig : Entity
    {
        public long CommanderBase { get; set; }
        public long ForceBase { get; set; }
        public long IntelligenceBase { get; set; }
        public long PoliticsBase { get; set; }
        public long CommanderGrow { get; set; }
        public long ForceGrow { get; set; }
        public long IntelligenceGrow { get; set; }
        public long PoliticsGrow { get; set; }

        public string ConfigName { get; set; } //变量的名字
    }
}
