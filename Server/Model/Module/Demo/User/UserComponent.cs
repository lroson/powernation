﻿using ETHotfix;
using System.Collections.Generic;

namespace ETModel
{
    [ObjectSystem]
    public class UserComponentAwakeSystem : AwakeSystem<UserComponent>
    {
        public override void Awake(UserComponent self)
        {
            self.Awake();
        }
    }
    /// <summary>
    /// 用户服用户管理组件
    /// </summary>
    public  class UserComponent : Component
    {
        public DBProxyComponent dbProxyComponent;
        public readonly Dictionary<long,User> OnlineUserDic=new Dictionary<long, User>();
        public static UserComponent Ins { private set; get; }
        public void Awake()
        {
            Ins = this;
            dbProxyComponent = Game.Scene.GetComponent<DBProxyComponent>();
        }

       


    }
}
