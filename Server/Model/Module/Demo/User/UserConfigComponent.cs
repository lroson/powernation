﻿using ETHotfix;
using System.Collections.Generic;

namespace ETModel
{
    [ObjectSystem]
    public class GameConfiguredComponentAwakeSystem : AwakeSystem<UserConfigComponent>
    {
        public override void Awake(UserConfigComponent self)
        {
            self.Awake();
        }
    }
    /// <summary>
    /// 数据库用户商品初始化配置
    /// </summary>
    public class UserConfigComponent : Component
    {
        public UserConfig InitUserCoins { private set; get; }
        public UserConfig InitUserGold { private set; get; }
        public long MaximumUserId { set; get; }
        public static UserConfigComponent Ins { private set; get; }

        private DBProxyComponent dbProxyComponent;

        public async void Awake()
        {
            Ins = this;
            dbProxyComponent = Game.Scene.GetComponent<DBProxyComponent>();
            //根据UserId排序查询，-1降序，count为最大UserId号查询的获取集合数量
            List<User> userIdMax = await dbProxyComponent.SortQuery<User>(user => true, user => user.UserId == -1, 1);
            if (userIdMax.Count > 0)
            {
                MaximumUserId = userIdMax[0].UserId;
            }
            else
            {
                MaximumUserId = 1000;
            }

            List<UserConfig> userConfigs = await dbProxyComponent.Query<UserConfig>(userConfig => true);
            if (userConfigs.Count == 0)
            {
                SaveConfigured();
            }
            else
            {
                SetConfigured(userConfigs);
            }

        }

        public void SetConfigured(List<UserConfig> userConfigs)
        {
            int count = 0;
            for (int i = 0; i < userConfigs.Count; i++)
            {
                switch (userConfigs[i].Id)
                {
                    case 1:
                        InitUserCoins = userConfigs[i];
                        count++;
                        break;
                    case 2:
                        InitUserGold = userConfigs[i];
                        count++;
                        break;
                    default:
                        Log.Error($"用户服没有该{userConfigs[i].Id}Id的配置");
                        break;
                }
            }
            if (count<2)
            {
                SaveConfigured();
            }
        }
        public async void SaveConfigured()
        {
            InitUserCoins = new UserConfig();
            InitUserCoins.Id = 1;
            InitUserCoins.ConfigName = "InitUserCoins";
            InitUserCoins.Value = 3000;
            await dbProxyComponent.Save(InitUserCoins);
            InitUserGold = new UserConfig();
            InitUserGold.Id = 2;
            InitUserGold.ConfigName = "InitUserGold";
            InitUserGold.Value = 20;
            await dbProxyComponent.Save(InitUserGold);
        }
    }
}
