﻿using ETHotfix;
using System.Collections.Generic;

namespace ETModel
{
    [ObjectSystem]
    public class CharacterConfigComponentAwakeSystem : AwakeSystem<CharacterConfigComponent>
    {
        public override void Awake(CharacterConfigComponent self)
        {
            self.Awake();
        }
    }
    /// <summary>
    /// 数据库用户商品初始化配置
    /// </summary>
    public class CharacterConfigComponent : Component
    {
        public CharacterConfig InitGeneralChar { private set; get; }
        public CharacterConfig InitWarriorChar { private set; get; }
        public CharacterConfig InitIntelligenceChar { private set; get; }
        public CharacterConfig InitPoliticianChar { private set; get; }
        public CharacterConfig InitAllroundChar { private set; get; }
        public long MaximumCharacterId { set; get; }
        public static CharacterConfigComponent Ins { private set; get; }

        private DBProxyComponent dbProxyComponent;

        public async void Awake()
        {
            Ins = this;
            dbProxyComponent = Game.Scene.GetComponent<DBProxyComponent>();
            //根据CharacterId排序查询，-1降序，count为最大UserId号查询的获取集合数量
            List<Character> characterIdMax = await dbProxyComponent.SortQuery<Character>(character => true, character => character.CharacterId == -1, 1);
            if (characterIdMax.Count > 0)
            {
                MaximumCharacterId = characterIdMax[0].CharacterId;
            }
            else
            {
                MaximumCharacterId = 1000;
            }

            List<CharacterConfig> characterConfigs = await dbProxyComponent.Query<CharacterConfig>(characterConfig => true);
            if (characterConfigs.Count == 0)
            {
                SaveConfigured();
            }
            else
            {
                SetConfigured(characterConfigs);
            }

        }

        public void SetConfigured(List<CharacterConfig> characterConfigs)
        {
            int count = 0;
            for (int i = 0; i < characterConfigs.Count; i++)
            {
                switch (characterConfigs[i].Id)
                {
                    case 1:
                        InitGeneralChar = characterConfigs[i];
                        count++;
                        break;
                    case 2:
                        InitWarriorChar = characterConfigs[i];
                        count++;
                        break;
                    case 3:
                        InitIntelligenceChar = characterConfigs[i];
                        count++;
                        break;
                    case 4:
                        InitPoliticianChar = characterConfigs[i];
                        count++;
                        break;
                    case 5:
                        InitAllroundChar = characterConfigs[i];
                        count++;
                        break;
                    default:
                        Log.Error($"用户服没有该{characterConfigs[i].Id}Id的配置");
                        break;
                }
            }
            if (count<5)
            {
                SaveConfigured();
            }
        }
        public async void SaveConfigured()
        {
            InitGeneralChar = new CharacterConfig();
            InitGeneralChar.Id = 1;
            InitGeneralChar.ConfigName = "InitGeneralChar";
            InitGeneralChar.CommanderBase = GeneralChar.CommanderBase;
            InitGeneralChar.ForceBase = GeneralChar.ForceBase;
            InitGeneralChar.IntelligenceBase = GeneralChar.IntelligenceBase;
            InitGeneralChar.PoliticsBase = GeneralChar.PoliticsBase;
            InitGeneralChar.CommanderGrow = GeneralChar.CommanderGrow;
            InitGeneralChar.ForceGrow = GeneralChar.ForceGrow;
            InitGeneralChar.IntelligenceGrow = GeneralChar.IntelligenceGrow;
            InitGeneralChar.PoliticsGrow = GeneralChar.PoliticsGrow;
            await dbProxyComponent.Save(InitGeneralChar);

            InitWarriorChar = new CharacterConfig();
            InitWarriorChar.Id = 2;
            InitWarriorChar.ConfigName = "InitWarriorChar";
            InitWarriorChar.CommanderBase = WarriorChar.CommanderBase;
            InitWarriorChar.ForceBase = WarriorChar.ForceBase;
            InitWarriorChar.IntelligenceBase = WarriorChar.IntelligenceBase;
            InitWarriorChar.PoliticsBase = WarriorChar.PoliticsBase;
            InitWarriorChar.CommanderGrow = WarriorChar.CommanderGrow;
            InitWarriorChar.ForceGrow = WarriorChar.ForceGrow;
            InitWarriorChar.IntelligenceGrow = WarriorChar.IntelligenceGrow;
            InitWarriorChar.PoliticsGrow = WarriorChar.PoliticsGrow;
            await dbProxyComponent.Save(InitWarriorChar);
            
            InitIntelligenceChar = new CharacterConfig();
            InitIntelligenceChar.Id = 3;
            InitIntelligenceChar.ConfigName = "InitIntelligenceChar";
            InitIntelligenceChar.CommanderBase = IntelligenceChar.CommanderBase;
            InitIntelligenceChar.ForceBase = IntelligenceChar.ForceBase;
            InitIntelligenceChar.IntelligenceBase = IntelligenceChar.IntelligenceBase;
            InitIntelligenceChar.PoliticsBase = IntelligenceChar.PoliticsBase;
            InitIntelligenceChar.CommanderGrow = IntelligenceChar.CommanderGrow;
            InitIntelligenceChar.ForceGrow = IntelligenceChar.ForceGrow;
            InitIntelligenceChar.IntelligenceGrow = IntelligenceChar.IntelligenceGrow;
            InitIntelligenceChar.PoliticsGrow = IntelligenceChar.PoliticsGrow;
            await dbProxyComponent.Save(InitIntelligenceChar);

            InitPoliticianChar = new CharacterConfig();
            InitPoliticianChar.Id = 4;
            InitPoliticianChar.ConfigName = "InitPoliticianChar";
            InitPoliticianChar.CommanderBase = PoliticianChar.CommanderBase;
            InitPoliticianChar.ForceBase = PoliticianChar.ForceBase;
            InitPoliticianChar.IntelligenceBase = PoliticianChar.IntelligenceBase;
            InitPoliticianChar.PoliticsBase = PoliticianChar.PoliticsBase;
            InitPoliticianChar.CommanderGrow = PoliticianChar.CommanderGrow;
            InitPoliticianChar.ForceGrow = PoliticianChar.ForceGrow;
            InitPoliticianChar.IntelligenceGrow = PoliticianChar.IntelligenceGrow;
            InitPoliticianChar.PoliticsGrow = PoliticianChar.PoliticsGrow;
            await dbProxyComponent.Save(InitPoliticianChar);

            InitAllroundChar = new CharacterConfig();
            InitAllroundChar.Id = 5;
            InitAllroundChar.ConfigName = "InitAllroundChar";
            InitAllroundChar.CommanderBase = AllroundChar.CommanderBase;
            InitAllroundChar.ForceBase = AllroundChar.ForceBase;
            InitAllroundChar.IntelligenceBase = AllroundChar.IntelligenceBase;
            InitAllroundChar.PoliticsBase = AllroundChar.PoliticsBase;
            InitAllroundChar.CommanderGrow = AllroundChar.CommanderGrow;
            InitAllroundChar.ForceGrow = AllroundChar.ForceGrow;
            InitAllroundChar.IntelligenceGrow = AllroundChar.IntelligenceGrow;
            InitAllroundChar.PoliticsGrow = AllroundChar.PoliticsGrow;
            await dbProxyComponent.Save(InitAllroundChar);
        }
    }
}
