﻿namespace ETModel
{
    /// <summary>
    /// Session心跳唤醒组件，但凡连接外网的时候会触发，所有外网的App必须添加HeartbeatMgrComponent,不然HeartbeatMgrComponent会报空
    /// </summary>
    [ObjectSystem]
    public class SessionHeartbeatAwakeSystem : AwakeSystem<Session, AChannel>
    {
        public override void Awake(Session self, AChannel b)
        {
            if (self.Network.AppType==AppType.None)//不是任何APPType 就是客户端
            {
                self.AddComponent<SessionHeartbeatComponent>();
            }
        }
    }
}

