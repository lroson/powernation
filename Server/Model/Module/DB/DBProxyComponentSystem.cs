﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;

namespace ETModel
{
    [ObjectSystem]
    public class DbProxyComponentSystem : AwakeSystem<DBProxyComponent>
    {
        public override void Awake(DBProxyComponent self)
        {
            self.Awake();
        }
    }

    /// <summary>
    /// 用来与数据库操作代理
    /// </summary>
    public static class DBProxyComponentEx
    {
        public static void Awake(this DBProxyComponent self)
        {
            StartConfig dbStartConfig = StartConfigComponent.Instance.DBConfig;
            self.dbAddress = dbStartConfig.GetComponent<InnerConfig>().IPEndPoint;
        }

        public static async ETTask Save(this DBProxyComponent self, ComponentWithId component)
        {
            Session session = Game.Scene.GetComponent<NetInnerComponent>().Get(self.dbAddress);
            await session.Call(new DBSaveRequest { Component = component });
        }

        public static async ETTask SaveBatch(this DBProxyComponent self, List<ComponentWithId> components)
        {
            Session session = Game.Scene.GetComponent<NetInnerComponent>().Get(self.dbAddress);
            await session.Call(new DBSaveBatchRequest { Components = components });
        }

        public static async ETTask Save(this DBProxyComponent self, ComponentWithId component, CancellationToken cancellationToken)
        {
            Session session = Game.Scene.GetComponent<NetInnerComponent>().Get(self.dbAddress);
            await session.Call(new DBSaveRequest { Component = component }, cancellationToken);
        }

        public static async ETVoid SaveLog(this DBProxyComponent self, ComponentWithId component)
        {
            Session session = Game.Scene.GetComponent<NetInnerComponent>().Get(self.dbAddress);
            await session.Call(new DBSaveRequest { Component = component, CollectionName = "Log" });
        }
        /// <summary>
        /// 按ID查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="id"></param>
        /// <returns></returns>
		public static ETTask<T> Query<T>(this DBProxyComponent self, long id) where T : ComponentWithId
        {
            string key = typeof(T).Name + id;
            ETTaskCompletionSource<T> tcs = new ETTaskCompletionSource<T>();
            if (self.TcsQueue.ContainsKey(key))
            {
                self.TcsQueue.Add(key, tcs);
                return tcs.Task;
            }

            self.TcsQueue.Add(key, tcs);
            self.QueryInner<T>(id, key).Coroutine();
            return tcs.Task;
        }
        /// <summary>
        /// 按ID和键查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static async ETVoid QueryInner<T>(this DBProxyComponent self, long id, string key) where T : ComponentWithId
        {
            try
            {
                Session session = Game.Scene.GetComponent<NetInnerComponent>().Get(self.dbAddress);
                DBQueryResponse dbQueryResponse = (DBQueryResponse)await session.Call(new DBQueryRequest { CollectionName = typeof(T).Name, Id = id });
                T result = (T)dbQueryResponse.Component;

                object[] tcss = self.TcsQueue.GetAll(key);
                self.TcsQueue.Remove(key);

                foreach (ETTaskCompletionSource<T> tcs in tcss)
                {
                    tcs.SetResult(result);
                }
            }
            catch (Exception e)
            {
                object[] tcss = self.TcsQueue.GetAll(key);
                self.TcsQueue.Remove(key);

                foreach (ETTaskCompletionSource<T> tcs in tcss)
                {
                    tcs.SetException(e);
                }
            }
        }

        /// <summary>
        /// 根据查询表达式查询
        /// </summary>
        /// <param name="self"></param>
        /// <param name="exp"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async ETTask<List<T>> Query<T>(this DBProxyComponent self, Expression<Func<T, bool>> exp) where T : ComponentWithId
        {
            ExpressionFilterDefinition<T> filter = new ExpressionFilterDefinition<T>(exp);
            IBsonSerializerRegistry serializerRegistry = BsonSerializer.SerializerRegistry;
            IBsonSerializer<T> documentSerializer = serializerRegistry.GetSerializer<T>();
            string json = filter.Render(documentSerializer, serializerRegistry).ToJson();
            var result = await self.Query<T>(json);
            return ConversionType<T>(result);

        }
        /// <summary>
        /// 按ID集合查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
		public static async ETTask<List<T>> Query<T>(this DBProxyComponent self, List<long> ids) where T : ComponentWithId
        {
            Session session = Game.Scene.GetComponent<NetInnerComponent>().Get(self.dbAddress);
            DBQueryBatchResponse dbQueryBatchResponse = (DBQueryBatchResponse)await session.Call(new DBQueryBatchRequest { CollectionName = typeof(T).Name, IdList = ids });
            return ConversionType<T>(dbQueryBatchResponse.Components);
        }

        /// <summary>
        /// 根据json查询条件查询
        /// </summary>
        /// <param name="self"></param>
        /// <param name="json"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static ETTask<List<ComponentWithId>> Query<T>(this DBProxyComponent self, string json) where T : ComponentWithId
        {
            string key = typeof(T).Name + json;
            ETTaskCompletionSource<List<ComponentWithId>> tcs = new ETTaskCompletionSource<List<ComponentWithId>>();
            if (self.TcsQueue.ContainsKey(key))
            {
                self.TcsQueue.Add(key, tcs);
                return tcs.Task;
            }

            self.TcsQueue.Add(key, tcs);
            self.QueryInner<T>(json, key).Coroutine();
            return tcs.Task;
        }

        private static async ETVoid QueryInner<T>(this DBProxyComponent self, string json, string key) where T : ComponentWithId
        {
            try
            {
                Session session = Game.Scene.GetComponent<NetInnerComponent>().Get(self.dbAddress);
                DBQueryJsonResponse dbQueryJsonResponse = (DBQueryJsonResponse)await session.Call(new DBQueryJsonRequest { CollectionName = typeof(T).Name, Json = json });
                var result = dbQueryJsonResponse.Components;

                object[] tcss = self.TcsQueue.GetAll(key);
                self.TcsQueue.Remove(key);

                foreach (ETTaskCompletionSource<List<ComponentWithId>> tcs in tcss)
                {
                    tcs.SetResult(result);
                }
            }
            catch (Exception e)
            {
                object[] tcss = self.TcsQueue.GetAll(key);
                self.TcsQueue.Remove(key);

                foreach (ETTaskCompletionSource<List<ComponentWithId>> tcs in tcss)
                {
                    tcs.SetException(e);
                }
            }
        }
        /// <summary>
        /// 根据表达式排序查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="queryExp">表达式</param>
        /// <param name="sortExp">根据该字段排序查询，1升序，-1降序</param>
        /// <param name="count">查询个数</param>
        /// <returns></returns>
        public static async ETTask<List<T>> SortQuery<T>(this DBProxyComponent self, Expression<Func<T, bool>> queryExp, Expression<Func<T, bool>> sortExp, int count) where T : ComponentWithId
        {
            string queryJson = ExpressionConversionJson(queryExp);
            string sortJson = ExpressionConversionJson(sortExp);
            return await self.SortQuery<T>(typeof(T).Name, queryJson, sortJson, count);
        }

        private static async ETTask<List<T>> SortQuery<T>(this DBProxyComponent self, string typeName, string queryJson, string sortJson, int count) where T : ComponentWithId
        {
            Session session = Game.Scene.GetComponent<NetInnerComponent>().Get(self.dbAddress);
            DBQueryJsonResponse dbQueryJsonResponse = (DBQueryJsonResponse)await session.Call(new DBSortQueryJsonRequest { CollectionName = typeName, QueryJson = queryJson, SortJson = sortJson, Count = count });
            return ConversionType<T>(dbQueryJsonResponse.Components);
        }
        private static List<T> ConversionType<T>(List<ComponentWithId> componentWithIds) where T : ComponentWithId
        {
            if (componentWithIds == null)
            {
                return null;
            }
            List<T> listt = new List<T>();
            foreach (var componentWithId in componentWithIds)
            {
                listt.Add(componentWithId as T);
            }
            return listt;
        }
        //Expression表达式转换时monongb语句
        private static string ExpressionConversionJson<T>(Expression<Func<T, bool>> exp)
        {
            ExpressionFilterDefinition<T> filter = new ExpressionFilterDefinition<T>(exp);
            IBsonSerializerRegistry serializerRegistry = BsonSerializer.SerializerRegistry;
            IBsonSerializer<T> documentSerializer = serializerRegistry.GetSerializer<T>();
            return filter.Render(documentSerializer, serializerRegistry).ToJson();
        }
    }
}