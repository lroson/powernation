﻿using System;
using System.Threading;
using ETModel;
using NLog;

namespace App
{
	internal static class Program
	{
		private static void Main(string[] args)
		{
			// 异步方法全部会回掉到主线程
			SynchronizationContext.SetSynchronizationContext(OneThreadSynchronizationContext.Instance);
			
			try
			{			
				Game.EventSystem.Add(DLLType.Model, typeof(Game).Assembly);
				Game.EventSystem.Add(DLLType.Hotfix, DllHelper.GetHotfixAssembly());

				Options options = Game.Scene.AddComponent<OptionComponent, string[]>(args).Options;
				StartConfig startConfig = Game.Scene.AddComponent<StartConfigComponent, string, int>(options.Config, options.AppId).StartConfig;

				if (!options.AppType.Is(startConfig.AppType))
				{
					Log.Error("命令行参数apptype与配置不一致");
					return;
				}

				IdGenerater.AppId = options.AppId;

				LogManager.Configuration.Variables["appType"] = $"{startConfig.AppType}";
				LogManager.Configuration.Variables["appId"] = $"{startConfig.AppId}";
				LogManager.Configuration.Variables["appTypeFormat"] = $"{startConfig.AppType, -8}";
				LogManager.Configuration.Variables["appIdFormat"] = $"{startConfig.AppId:0000}";

				Log.Info($"server start........................ {startConfig.AppId} {startConfig.AppType}");

				Game.Scene.AddComponent<TimerComponent>();
				Game.Scene.AddComponent<OpcodeTypeComponent>();
				Game.Scene.AddComponent<MessageDispatcherComponent>();
			    // 配置管理，配置文件读取组件
                Game.Scene.AddComponent<ConfigComponent>();
			    //获取服务器内部之间的Session
                Game.Scene.AddComponent<NetInnerSessionComponent>();

                // 根据不同的AppType添加不同的组件
                OuterConfig outerConfig = startConfig.GetComponent<OuterConfig>();
				InnerConfig innerConfig = startConfig.GetComponent<InnerConfig>();
				ClientConfig clientConfig = startConfig.GetComponent<ClientConfig>();

                switch (startConfig.AppType)
				{
					case AppType.Manager:
						Game.Scene.AddComponent<AppManagerComponent>();
					    Game.Scene.AddComponent<ConsoleComponent>();
                        Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
						Game.Scene.AddComponent<NetOuterComponent, string>(outerConfig.Address);
					    Game.Scene.AddComponent<HeartbeatMgrComponent>();//心跳管理组件 凡是有NetOuterComponent组件的必加，管理服、验证服 也是要加的
                        break;
					case AppType.Realm:
                        Game.Scene.AddComponent<MailboxDispatcherComponent>();
						Game.Scene.AddComponent<ActorMessageDispatcherComponent>();
						Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
						Game.Scene.AddComponent<NetOuterComponent, string>(outerConfig.Address);
						Game.Scene.AddComponent<LocationProxyComponent>();
						Game.Scene.AddComponent<RealmGateAddressComponent>();
					    Game.Scene.AddComponent<DBProxyComponent>();
					    Game.Scene.AddComponent<HeartbeatMgrComponent>();//心跳管理组件 凡是有NetOuterComponent组件的必加，管理服、验证服 也是要加的
                        break;
					case AppType.Gate:
						Game.Scene.AddComponent<MailboxDispatcherComponent>();
						Game.Scene.AddComponent<ActorMessageDispatcherComponent>();
						Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
						Game.Scene.AddComponent<NetOuterComponent, string>(outerConfig.Address);
						Game.Scene.AddComponent<LocationProxyComponent>();
						Game.Scene.AddComponent<ActorMessageSenderComponent>();
						Game.Scene.AddComponent<ActorLocationSenderComponent>();
						Game.Scene.AddComponent<GateSessionKeyComponent>();
					    Game.Scene.AddComponent<DBProxyComponent>();
					    Game.Scene.AddComponent<CoroutineLockComponent>();
					    Game.Scene.AddComponent<GateUserComponent>();//网关管理用户的组件
					    Game.Scene.AddComponent<HeartbeatMgrComponent>();//心跳管理组件 凡是有NetOuterComponent组件的必加，管理服、验证服 也是要加的
                        break;
					case AppType.Location:
						Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
						Game.Scene.AddComponent<LocationComponent>();
					    Game.Scene.AddComponent<CoroutineLockComponent>();
                        break;
					case AppType.Map:
						Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
						Game.Scene.AddComponent<UnitComponent>();
						Game.Scene.AddComponent<LocationProxyComponent>();
						Game.Scene.AddComponent<ActorMessageSenderComponent>();
						Game.Scene.AddComponent<ActorLocationSenderComponent>();
						Game.Scene.AddComponent<MailboxDispatcherComponent>();
						Game.Scene.AddComponent<ActorMessageDispatcherComponent>();
						Game.Scene.AddComponent<PathfindingComponent>();
					    Game.Scene.AddComponent<DBProxyComponent>();
					    Game.Scene.AddComponent<CoroutineLockComponent>();
                        break;
				    case AppType.User:
                        //用户服
				        Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
				        Game.Scene.AddComponent<LocationProxyComponent>();
				        Game.Scene.AddComponent<DBProxyComponent>();
                        Game.Scene.AddComponent<UserComponent>();//用户服管理组件
				        Game.Scene.AddComponent<UserConfigComponent>();//用户配置组件
				        Game.Scene.AddComponent<CharacterConfigComponent>();//角色配置组件
                        break;
				    case AppType.Match:
                        //匹配服
				        Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
				        Game.Scene.AddComponent<LocationProxyComponent>();
                        //Game.Scene.AddComponent<GameMatchRoomConfigComponent>();//匹配游戏房间配置表
                        //Game.Scene.AddComponent<MatchRoomComponent>();//匹配服房间组件
                        break;
                    case AppType.DB:
				        Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
				        Game.Scene.AddComponent<DBComponent>();
                        break;
                    case AppType.AllServer:
						// 发送普通actor消息
						Game.Scene.AddComponent<ActorMessageSenderComponent>();
						
						// 发送location actor消息
						Game.Scene.AddComponent<ActorLocationSenderComponent>();
						
						Game.Scene.AddComponent<DBComponent>();
					    
						Game.Scene.AddComponent<DBProxyComponent>();
						
						// location server需要的组件
						Game.Scene.AddComponent<LocationComponent>();
						
						// 访问location server的组件
						Game.Scene.AddComponent<LocationProxyComponent>();
						
						// 这两个组件是处理actor消息使用的
						Game.Scene.AddComponent<MailboxDispatcherComponent>();
						Game.Scene.AddComponent<ActorMessageDispatcherComponent>();
						
						// 内网消息组件
						Game.Scene.AddComponent<NetInnerComponent, string>(innerConfig.Address);
						
						// 外网消息组件
						Game.Scene.AddComponent<NetOuterComponent, string>(outerConfig.Address);
						
						// manager server组件，用来管理其它进程使用
						Game.Scene.AddComponent<AppManagerComponent>();
                        Game.Scene.AddComponent<ConsoleComponent>();
                        Game.Scene.AddComponent<CoroutineLockComponent>();
                        Game.Scene.AddComponent<RealmGateAddressComponent>();
						Game.Scene.AddComponent<GateSessionKeyComponent>();
                        Game.Scene.AddComponent<GateUserComponent>();//网关管理用户的组件

                        // recast寻路组件
                        Game.Scene.AddComponent<PathfindingComponent>();
						
						Game.Scene.AddComponent<UnitComponent>();

                        Game.Scene.AddComponent<UserComponent>();//用户服管理组件
                        Game.Scene.AddComponent<UserConfigComponent>();//用户配置组件
                        Game.Scene.AddComponent<CharacterConfigComponent>();//角色配置组件
                        Game.Scene.AddComponent<HeartbeatMgrComponent>();//心跳管理组件 验证服 也是要加的
                        //Game.Scene.AddComponent<GameMatchRoomConfigComponent>();//匹配游戏房间配置表
                        //Game.Scene.AddComponent<MatchRoomComponent>();//匹配服房间组件

                        // Game.Scene.AddComponent<HttpComponent>();
                        break;
					case AppType.Benchmark:
						Game.Scene.AddComponent<NetOuterComponent>();
						Game.Scene.AddComponent<BenchmarkComponent, string>(clientConfig.Address);
						break;
					case AppType.BenchmarkWebsocketServer:
						Game.Scene.AddComponent<NetOuterComponent, string>(outerConfig.Address);
						break;
					case AppType.BenchmarkWebsocketClient:
						Game.Scene.AddComponent<NetOuterComponent>();
						Game.Scene.AddComponent<WebSocketBenchmarkComponent, string>(clientConfig.Address);
						break;
					default:
						throw new Exception($"命令行参数没有设置正确的AppType: {startConfig.AppType}");
				}
				
				while (true)
				{
					try
					{
						Thread.Sleep(1);
						OneThreadSynchronizationContext.Instance.Update();
						Game.EventSystem.Update();
					}
					catch (Exception e)
					{
						Log.Error(e);
					}
				}
			}
			catch (Exception e)
			{
				Log.Error(e);
			}
		}
	}
}
