﻿namespace ETEditor
{
    public static class GetSelectionGoType
    {
        public static string GetFileType(string path)
        {
            if (path.Contains(ToyGameType.Lobby))
            {
                return ToyGameType.Lobby;
            }
            if (path.Contains(ToyGameType.JoyLandlords))
            {
                return ToyGameType.JoyLandlords;
            }
            if (path.Contains(ToyGameType.CardFiveStar))
            {
                return ToyGameType.CardFiveStar;
            }
            return "";
        }
    }
}
