﻿using System;

namespace ETModel
{
    [ObjectSystem]
	public class UiBundleDownloaderComponentAwakeSystem : AwakeSystem<BundleDownloaderComponent>
	{
		public override void Awake(BundleDownloaderComponent self)
		{
			//self.bundles = new Queue<string>();
			//self.downloadedBundles = new HashSet<string>();
			//self.downloadingBundle = "";
		}
	}

    public class BundleDownSchedule
    {
        public Action FinishAndDisposeCall;
        public Action<string> LoserCall;
        private BundleDownloader BundleDownloader;
        public bool IsFinishDown;
        public int Progress
        {
            get
            {
                if (BundleDownloader == null)
                {
                    return 0;
                }
                return BundleDownloader.Progress;
            }
        }

        public void SetBundleDownloader(BundleDownloader bundleDownloader)
        {
            BundleDownloader = bundleDownloader;
            BundleDownloader.pFinishAndDisposeCall += FinishAndDisposeCall;
            BundleDownloader.pFinishAndDisposeCall += FinishAciton;
            BundleDownloader.pLoserCall += LoserCall;
        }

        private void FinishAciton()
        {
            IsFinishDown = true;
        }
    }
    /// <summary>
    /// 用来对比web端的资源，比较md5，对比下载资源
    /// </summary>
    public class BundleDownloaderComponent : Component
	{
	    //开始下载
	    public async ETTask StartAsync(ToyGameVersions toyGameVersions, BundleDownSchedule downloader = null)
	    {
	        await StartAsync(toyGameVersions.DownloadFolder, toyGameVersions.VersionFile, downloader);
	    }
	    //开始下载
	    public async ETTask StartAsync(string fileFoldr = "", string versionName = "Version.txt", BundleDownSchedule downloader = null)
	    {
	        BundleDownloader bundleDownloader = ComponentFactory.Create<BundleDownloader>();
	        downloader?.SetBundleDownloader(bundleDownloader);
	        await bundleDownloader.StartAsync(fileFoldr, versionName);
	        //销毁bundle下载工具对象
            bundleDownloader.Dispose();
	    }

        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            if (this.Entity.IsDisposed)
            {
                return;
            }
            base.Dispose();

            this.Entity.RemoveComponent<BundleDownloaderComponent>();
        }

        //ET项目，已移往BundleDownloader

    }
}
