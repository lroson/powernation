﻿using System.Net;

namespace ETModel
{
	public static class NetworkHelper
	{
		public static IPEndPoint ToIPEndPoint(string host, int port)
		{
			return new IPEndPoint(IPAddress.Parse(host), port);
		}
        /// <summary>
        /// ip转EndPoint
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
		public static IPEndPoint ToIPEndPoint(string address)
		{
			int index = address.LastIndexOf(':');
			string host = address.Substring(0, index);
			string p = address.Substring(index + 1);
			int port = int.Parse(p);
			return ToIPEndPoint(host, port);
		}
	    /// <summary>
	    /// 网址转IPEndPoint
	    /// </summary>
	    /// <param name="address"></param>
	    /// <returns></returns>
	    public static IPEndPoint DomainToIPEndPoint(string address)
	    {
	        int index = address.LastIndexOf(':');
	        string host = address.Substring(0, index);

	        IPAddress[] addresses = Dns.GetHostAddresses(host);
	        host = addresses[0].ToString();
	        string p = address.Substring(index + 1);
	        int port = int.Parse(p);
	        return ToIPEndPoint(host, port);
	    }
    }
}
