﻿using System;

namespace ETModel
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GameAisleAttribute : BaseAttribute
    {
        public long Type { get; protected set; }

        public GameAisleAttribute(long type)
        {
            this.Type = type;
        }
    }
}

