﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ETModel
{
    /// <summary>
    /// UGUI点击事件注册
    /// </summary>
	public static class ActionHelper
	{
		public static void Add(this Button.ButtonClickedEvent buttonClickedEvent, Action action)
		{
			buttonClickedEvent.AddListener(() => { action(); });
		}
	    public static void Add(this Button button, Action action, bool isAnim = true)
	    {
	        button.onClick.RemoveAllListeners();
	        button.onClick.AddListener(() => { action(); });
	        if (isAnim && button.GetComponent<ButtonAnimationEffect>() == null)
	        {
	            button.transition = Selectable.Transition.None;
	            button.gameObject.AddComponent<ButtonAnimationEffect>();
	        }
	    }

	    public static void Add(this Toggle toggle, Action<bool> action)
	    {
	        toggle.graphic.gameObject.SetActive(toggle.isOn);
	        toggle.onValueChanged.RemoveAllListeners();
	        toggle.onValueChanged.AddListener((bol) =>
	        {
	            toggle.graphic.gameObject.SetActive(toggle.isOn);
	            action(bol);
	        });
	    }

	    public static void Add(this Slider slider, Action<float> action)
	    {
	        slider.onValueChanged.RemoveAllListeners();
	        slider.onValueChanged.AddListener((value) => { action(value); });
	    }
        
    }
    /// <summary>
    /// UGUI点击放大动画效果
    /// </summary>
    public class ButtonAnimationEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler//EventTrigger包含所有的
    {
        private static Vector3 downScale = new Vector3(1.1f, 1.1f, 1.1f);
        private static Vector3 upScale = Vector3.one;

        public static Action ButtonPointerUpAction;//按钮抬起事件
        public void OnPointerDown(PointerEventData eventData)
        {
            //Debug.Log("按下" + this.gameObject.name);
            this.transform.localScale = downScale;
        }
        public void OnPointerUp(PointerEventData eventData)
        {
            ButtonPointerUpAction?.Invoke();
            //Debug.Log("抬起" + this.gameObject.name);
            this.transform.localScale = upScale;
        }
    }
}