﻿using ETModel;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace ETHotfix
{
    [ObjectSystem]
    public class UIComponentAwakeSystem : AwakeSystem<UIComponent>
    {
        public override void Awake(UIComponent self)
        {
            self.GameObject.layer = LayerMask.NameToLayer(LayerNames.UI);
            self.Camera = Component.Global.transform.Find("UICamera").gameObject;
            Canvas canvas = self.GameObject.AddComponent<Canvas>();
            //canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = self.Camera.GetComponent<Camera>();
            CanvasScaler canvasScaler = self.GameObject.AddComponent<CanvasScaler>();
            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            canvasScaler.referenceResolution = new Vector2(1920, 1080);
            canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
            canvasScaler.matchWidthOrHeight = 0.5f;
            self.GameObject.AddComponent<GraphicRaycaster>();
        }
    }

    /// <summary>
    /// 管理所有UI
    /// </summary>
    public class UIComponent : Component
    {
        public GameObject Camera;

        public Dictionary<string, UI> uis = new Dictionary<string, UI>();

        public void Add(UI ui)
        {
            //ui.GameObject.GetComponent<Canvas>().worldCamera = this.Camera.GetComponent<Camera>();

            this.uis.Add(ui.Name, ui);
            ui.Parent = this;
        }

        public void Remove(string name)
        {
            if (!this.uis.TryGetValue(name, out UI ui))
            {
                return;
            }
            this.uis.Remove(name);
            ui.Dispose();
        }
        public void RemoveAll()
        {
            if (this.uis.Count > 0)
            {
                string[] tmp = this.uis.Keys.ToArray();
                foreach (var s in tmp)
                {
                    Remove(s);
                }
            }
        }

        public UI Get(string name)
        {
            UI ui = null;
            if (this.uis.TryGetValue(name, out ui))
            {
                return ui;
            }
            return null;
        }

        public string[] GetAllName()
        {
            return this.uis.Keys.ToArray();
        }
    }
}