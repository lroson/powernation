﻿namespace ETHotfix
{
    public static class UIType
    {
	    public const string Root = "Root";
	    public const string UILogin = "UILogin";
        public const string UILobbyPanel = "UILobbyPanel";
        public const string UIWarning = "UIWarning";
        public const string UIConnectLoading = "UIConnectLoading";
        public const string Avatar = "Avatar";
    }
}