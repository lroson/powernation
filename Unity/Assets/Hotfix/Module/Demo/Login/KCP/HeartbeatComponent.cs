﻿using System;
using ETModel;
using System.Net.Sockets;

namespace ETHotfix
{
    [ObjectSystem]
    public class HeartbeatComponentAwakeSystem : AwakeSystem<HeartbeatComponent>
    {

        public override void Awake(HeartbeatComponent self)
        {
            self.Awake().Coroutine();
        }
    }
    
    /// <summary>
    /// 心跳组件
    /// </summary>
    public class HeartbeatComponent : Component
    {

        public async ETVoid Awake()
        {
            Session session = (this.Entity as Session);
            DetectionNetworkType(session);//检测网络连接状态
            while (!this.IsDisposed)
            {
                await ETModel.Game.Scene.GetComponent<TimerComponent>().WaitAsync(10000);
                session.Send(new C2G_Heartbeat());//每间隔10秒发一条 心跳消息
            }
        }
        
        public async void DetectionNetworkType(Session session)
        {
            while (!session.IsDisposed)
            {
                await ETModel.Game.Scene.GetComponent<TimerComponent>().WaitAsync(1000);
                //如果当前是无网络状态 发送连接失败的消息
                if (NetworkType.None == SdkMgr.Ins.GetNetworkInfo())
                {
                    session.session.Error = (int)SocketError.NetworkDown;
                    session.Dispose();
                }
            }
        }
    }
}
