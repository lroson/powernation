﻿using ETModel;
using System;

namespace ETHotfix
{
    public static class KCPNetWorkState
    {
        public const string BebeingConnect = "BebeingConnect";
        public const string Disconnect = "Disconnect";
        public const string Connected = "Connected";
    }
    [ObjectSystem]
    public class KCPConnectStateEventAwakeSystem : AwakeSystem<KCPStateManage>
    {
        public override void Awake(KCPStateManage self)
        {
            self.Awake();
        }
    }
    public  class KCPStateManage:Component
    {
        public string KCPNetWorkState {  set; get; }

        public Action StartConnectCall;//开始连接
        public Action<G2C_GateLogin> ConnectSuccessCall;//连接成功
        public Action ConnectFailureCall; //连接失败
        public Action ConnectLostCall;//连接断开
        public Action StartReconnectionCall;//开始重连
        public Action<G2C_GateLogin> AgainConnectSuccessCall;//重连成功
        public Action AgainConnectFailureCall;//重连失败
        public Action InitiativeDisconnectCall;//主动断开连接

        //单例模式
        public static KCPStateManage Ins { private set; get; }

        public void Awake()
        {
            Ins = this;
        }
        //开始连接
        public void StartConnect()
        {
            Log.Debug("开始连接");
            KCPNetWorkState = ETHotfix.KCPNetWorkState.BebeingConnect;
            StartConnectCall?.Invoke();//调用弹出连接旋转圈圈事件
        }
        //连接成功
        public void ConnectSuccess(G2C_GateLogin g2CGateLogin)
        {
            Log.Debug("连接成功");
            KCPNetWorkState = ETHotfix.KCPNetWorkState.Connected;
            ConnectSuccessCall?.Invoke(g2CGateLogin);
        }

        //连接失败
        public void ConnectFailure()
        {
            Log.Debug("连接失败");
            KCPNetWorkState = ETHotfix.KCPNetWorkState.Disconnect;
            ConnectFailureCall?.Invoke();
        }

        //连接断开
        public async void ConnectLost()
        {
            Log.Debug("连接断开");
            KCPNetWorkState = ETHotfix.KCPNetWorkState.Disconnect;
            await ETModel.Game.Scene.GetComponent<TimerComponent>().WaitAsync(1000);//连接断开后等待1秒 才发起重连 因为底层需要对Session进行一些处理
            ConnectLostCall?.Invoke();
        }

        //开始重连
        public void StartReconnection()
        {
            Log.Debug("开始重连");
            KCPNetWorkState = ETHotfix.KCPNetWorkState.BebeingConnect;
            //StartReconnectionCall?.Invoke();
        }
        //重连成功
        public void AgainConnectSuccess(G2C_GateLogin g2CGateLogin)
        {
            Log.Debug("重连成功");
            KCPNetWorkState = ETHotfix.KCPNetWorkState.Connected;
            AgainConnectSuccessCall?.Invoke(g2CGateLogin);
        }
        //重连失败
        public void AgainConnectFailure()
        {

            Log.Debug("重连失败");
            KCPNetWorkState = ETHotfix.KCPNetWorkState.Disconnect;
            AgainConnectFailureCall?.Invoke();

        }
        //主动断开连接
        public void DisconnectInitiative()
        {
            Log.Debug("主动连接断开");
            KCPNetWorkState = ETHotfix.KCPNetWorkState.Disconnect;
            InitiativeDisconnectCall?.Invoke();
        }
    }
}
