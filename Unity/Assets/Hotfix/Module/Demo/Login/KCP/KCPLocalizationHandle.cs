using ETModel;
using System;

namespace ETHotfix
{
    [ObjectSystem]
    public class KCPLocalizationHandleAwakeSystem : AwakeSystem<KCPLocalizationHandle>
    {
        public override void Awake(KCPLocalizationHandle self)
        {
            self.Awake();
        }
    }
    /// <summary>
    /// KCP本地处理，注册连接、断线、重连、连接失败弹窗和返回登录界面
    /// </summary>
    public class KCPLocalizationHandle : Component
    {
        private Action<bool> action;
        public void Awake()
        {
            KCPStateManage.Ins.ConnectLostCall += ConnectLostEvent;
            KCPStateManage.Ins.AgainConnectFailureCall += AgainConnectFailureEvent;

            KCPStateManage.Ins.StartConnectCall += StartConnectEvent;
            KCPStateManage.Ins.StartReconnectionCall += StartReconnectionEvent;
            KCPStateManage.Ins.ConnectFailureCall += ConnectFailureEvent;
            KCPStateManage.Ins.ConnectSuccessCall += ConnectSuccessEvent;
            KCPStateManage.Ins.AgainConnectSuccessCall += AgainConnectSuccessEvent;
        }

        //开始连接
        private void StartConnectEvent()
        {
            //UIComponent.GetUiView<LoadingIconPanelComponent>().Show();
            Game.EventSystem.Run(EventIdType.CreateConnectLoadingUI);
        }
        //开始重连
        private void StartReconnectionEvent()
        {
            //UIComponent.GetUiView<LoadingIconPanelComponent>().Show();
            Game.EventSystem.Run(EventIdType.CreateConnectLoadingUI);
        }
        //连接失败
        private void ConnectFailureEvent()
        {
            //UIComponent.GetUiView<LoadingIconPanelComponent>().Hide();
            Game.EventSystem.Run(EventIdType.RemoveConnectLoadingUI);
            //UIComponent.GetUiView<PopUpHintPanelComponent>().ShowOptionWindow("网络连接失败是否重试", ConnectFailureConift);
            this.action = this.ConnectFailureConift;
            WarningHelper.WarningWithAction("重新连接失败是否重试!", this.action);
        }
        //连接失败进行登录重连
        public void ConnectFailureConift(bool bol)
        {
            if (bol)
            {
                KCPUseManage.Ins.AgainLoginAndConnect();
            }
            else
            {
                Game.Scene.GetComponent<GameAisleComponent>().StartGame(GameAisleId.Login);
            }
        }
        //连接成功
        private void ConnectSuccessEvent(G2C_GateLogin g2CGateLogin)
        {
            //UIComponent.GetUiView<LoadingIconPanelComponent>().Hide();
            Game.EventSystem.Run(EventIdType.RemoveConnectLoadingUI);
            User user = g2CGateLogin.User;
            Game.Scene.GetComponent<UserComponent>().SetSelfUser(user);
            Game.Scene.GetComponent<GameAisleComponent>().StartGame(GameAisleId.Lobby);
            SessionComponent.Instance.Session.AddComponent<HeartbeatComponent>();//添加心跳组件 
        }
        //重连成功
        private void AgainConnectSuccessEvent(G2C_GateLogin g2CGateLogin)
        {
            //UIComponent.GetUiView<LoadingIconPanelComponent>().Hide();
            Game.EventSystem.Run(EventIdType.RemoveConnectLoadingUI);
            SessionComponent.Instance.Session.AddComponent<HeartbeatComponent>();//添加心跳组件 
        }
        //连接断开
        private void ConnectLostEvent()
        {
            KCPUseManage.Ins.Reconnection();
        }
        //重连失败后弹窗
        private void AgainConnectFailureEvent()
        {
            //UIComponent.GetUiView<LoadingIconPanelComponent>().Hide();
            Game.EventSystem.Run(EventIdType.RemoveConnectLoadingUI);
            //UIComponent.GetUiView<PopUpHintPanelComponent>().ShowOptionWindow("重新连接失败是否重试", (bol) =>
            this.action = this.ReconnectAction;
            WarningHelper.WarningWithAction("重新连接失败是否重试!", this.action);
        }
        //重连失败进行重连
        void ReconnectAction(bool isReconnect)
        {
            if (isReconnect)
            {
                KCPUseManage.Ins.Reconnection();
            }
            else
            {
                Game.Scene.GetComponent<GameAisleComponent>().StartGame(GameAisleId.Login);
            }
        }
    }
}
