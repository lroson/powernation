﻿using ETModel;

namespace ETHotfix
{
	[Event(EventIdType.RemoveLobbyUI)]
	public class RemoveLobbyUI : AEvent
	{
		public override void Run()
		{
			Game.Scene.GetComponent<UIComponent>().Remove(UIType.UILobbyPanel);
			ETModel.Game.Scene.GetComponent<ResourcesComponent>().UnloadBundle(UIType.UILobbyPanel.StringToAB());
		}
	}
}
