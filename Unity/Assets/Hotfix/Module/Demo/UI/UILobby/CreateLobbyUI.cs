﻿using ETModel;

namespace ETHotfix
{
	[Event(EventIdType.CreateLobbyUI)]
	public class CreateLobbyUI : AEvent
	{
		public override void Run()
		{
			UI ui = UILobbyFactory.Create();
			Game.Scene.GetComponent<UIComponent>().Add(ui);
		}
	}
}
