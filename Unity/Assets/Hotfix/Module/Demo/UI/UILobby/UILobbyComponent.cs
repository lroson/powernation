﻿using ETModel;
using UnityEngine;
using UnityEngine.UI;

namespace ETHotfix
{
    [ObjectSystem]
	public class UiLobbyComponentSystem : AwakeSystem<UILobbyComponent>
	{
		public override void Awake(UILobbyComponent self)
		{
			self.Awake();
		}
	}
	
	public class UILobbyComponent : Component
	{
		private GameObject enterMap;

		public void Awake()
		{
			ReferenceCollector rc = this.GetParent<UI>().GameObject.GetComponent<ReferenceCollector>();
			
			enterMap = rc.Get<GameObject>("EnterMap");
			enterMap.GetComponent<Button>().onClick.Add(this.EnterMap);
            //获取用户的角色信息，如果CharId=0则没有角色，需新建角色
		    if (Game.Scene.AddComponent<UserComponent>().SelfUser.CharacterId == 0)
		    {
		        
		    }
		}

		private void EnterMap()
		{
			MapHelper.EnterMapAsync().Coroutine();
		}
	}
}
