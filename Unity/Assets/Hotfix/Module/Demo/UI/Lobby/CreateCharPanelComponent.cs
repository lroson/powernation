﻿using System;
using ETModel;
using UnityEngine;
using UnityEngine.UI;

namespace ETHotfix
{
    [ObjectSystem]
    public class CreateCharPanelComponentSystem : AwakeSystem<CreateCharPanelComponent>
    {
        public override void Awake(CreateCharPanelComponent self)
        {
            self.Awake();
        }
    }
    public class CreateCharPanelComponent : Component
    {
        #region 脚本工具生成的代码
        private Image characterImage;
        private Dropdown selectSexDrop;
        private Dropdown selectCharTypeDrop;
        private Button confirmBtn;
        private Text commandText;
        private Text commandGrowText;
        private Text forceText;
        private Text forceGrowText;
        private Text intelligenceText;
        private Text intelligenceGrowText;
        private Text politicsText;
        private Text politicsGrowText;
        private Text spearText;
        private Text cavalryText;
        private Text archText;
        private Text firstTacticsText;
        private Text secondTacticsText;
        private Text thirdTacticsText;
        public void Awake()
        {
            ReferenceCollector rc = this.GetParent<UI>().GameObject.GetComponent<ReferenceCollector>();
            characterImage = rc.Get<GameObject>("characterImage").GetComponent<Image>();
            selectSexDrop = rc.Get<GameObject>("selectSexDrop").GetComponent<Dropdown>();
            selectCharTypeDrop = rc.Get<GameObject>("selectCharTypeDrop").GetComponent<Dropdown>();
            confirmBtn = rc.Get<GameObject>("confirmBtn").GetComponent<Button>();
            confirmBtn.Add(ConfirmBtnEvent);
            commandText = rc.Get<GameObject>("commandText").GetComponent<Text>();
            commandGrowText = rc.Get<GameObject>("commandGrowText").GetComponent<Text>();
            forceText = rc.Get<GameObject>("forceText").GetComponent<Text>();
            forceGrowText = rc.Get<GameObject>("forceGrowText").GetComponent<Text>();
            intelligenceText = rc.Get<GameObject>("intelligenceText").GetComponent<Text>();
            intelligenceGrowText = rc.Get<GameObject>("intelligenceGrowText").GetComponent<Text>();
            politicsText = rc.Get<GameObject>("politicsText").GetComponent<Text>();
            politicsGrowText = rc.Get<GameObject>("politicsGrowText").GetComponent<Text>();
            spearText = rc.Get<GameObject>("spearText").GetComponent<Text>();
            cavalryText = rc.Get<GameObject>("cavalryText").GetComponent<Text>();
            archText = rc.Get<GameObject>("archText").GetComponent<Text>();
            firstTacticsText = rc.Get<GameObject>("firstTacticsText").GetComponent<Text>();
            secondTacticsText = rc.Get<GameObject>("secondTacticsText").GetComponent<Text>();
            thirdTacticsText = rc.Get<GameObject>("thirdTacticsText").GetComponent<Text>();
            InitPanel();
        }

        private void ConfirmBtnEvent()
        {
            throw new NotImplementedException();
        }
        #endregion
        private void InitPanel()
        {
        }
    }
}
