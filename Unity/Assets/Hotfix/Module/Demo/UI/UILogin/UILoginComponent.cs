﻿using System;
using ETModel;
using UnityEngine;
using UnityEngine.UI;

namespace ETHotfix
{
    [ObjectSystem]
    public class UiLoginComponentSystem : AwakeSystem<UILoginComponent>
    {
        public override void Awake(UILoginComponent self)
        {
            self.Awake();
        }
    }

    public class UILoginComponent : Component
    {
        private Button BtnTouristLogin;
        private Button BtnWechatLogin;
        private Toggle ToggleAgreement;
        private InputField ToursitAccount;
        private GameObject ImageTouristPanel;

        public void Awake()
        {
            ReferenceCollector rc = this.GetParent<UI>().GameObject.GetComponent<ReferenceCollector>();
            BtnTouristLogin = rc.Get<GameObject>("BtnTouristLogin").GetComponent<Button>();
            BtnTouristLogin.Add(BtnTouristLoginEvent);
            BtnWechatLogin = rc.Get<GameObject>("BtnWechatLogin").GetComponent<Button>();
            BtnWechatLogin.Add(WeChatLoginBtnEvent);
            ToggleAgreement = rc.Get<GameObject>("ToggleAgreement").GetComponent<Toggle>();
            ToursitAccount = rc.Get<GameObject>("ToursitAccount").GetComponent<InputField>();
            ImageTouristPanel = rc.Get<GameObject>("ImageTouristPanel");
            InitPanel();
        }
        public void InitPanel()
        {
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
            {
                ImageTouristPanel.SetActive(true);
            }
            else
            {
                ImageTouristPanel.SetActive(false);
            }

            if (Application.isMobilePlatform && PlayerPrefs.HasKey(GlobalConstant.LoginVoucher))
            {
                string loginVoucher = PlayerPrefs.GetString(GlobalConstant.LoginVoucher, string.Empty);
                if (!string.IsNullOrEmpty(loginVoucher))
                {
                    Game.Scene.GetComponent<KCPUseManage>().LoginAndConnect(LoginType.Voucher, loginVoucher);//如果记录有凭证 直接发送凭证登陆
                }
            }
        }

        private void WeChatLoginBtnEvent()
        {
            if (!ToggleAgreement.isOn)
            {
                ShowAgreeHint();
                return;
            }
            SdkCall.Ins.WeChatLoginAction = WeChatLogin;//微信回调
            SdkMgr.Ins.WeChatLogin();//发起微信登陆
        }

        public void WeChatLogin(string message)
        {
            KCPUseManage.Ins.LoginAndConnect(LoginType.WeChat, message);
        }

        public async void ShowAgreeHint()
        {
            await WarningHelper.WarningAsync("请勾选协议!",true);
        }

        private void BtnTouristLoginEvent()
        {
            try
            {
                if (string.IsNullOrEmpty(ToursitAccount.text))
                {
                    ToursitAccount.text = "0";
                }
                Game.Scene.GetComponent<KCPUseManage>().LoginAndConnect(LoginType.Editor, ToursitAccount.text);
            }
            catch (Exception e)
            {
                Log.Error("登陆失败" + e);
                throw;
            }

        }
    }
}
