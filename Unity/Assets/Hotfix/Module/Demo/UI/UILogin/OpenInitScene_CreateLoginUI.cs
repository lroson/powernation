﻿using ETModel;

namespace ETHotfix
{
	[Event(EventIdType.OpenInitScene)]
	public class OpenInitScene_CreateLoginUI : AEvent
	{
		public override void Run()
		{
			UI ui = UILoginFactory.Create();
			Game.Scene.GetComponent<UIComponent>().Add(ui);
		}
	}
}
