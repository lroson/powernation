﻿using ETModel;
using UnityEngine;

namespace ETHotfix
{
    [Event(EventIdType.CreateConnectLoadingUI)]
    public class CreateConnectLoadingUI : AEvent
    {
        public override void Run()
        {
            UI ui = UIConnectLoadingFactory.Create();
            Game.Scene.GetComponent<UIComponent>().Add(ui);
        }
    }
}
