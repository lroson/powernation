﻿using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class UIConnectLoadingComponentAwakeSystem : AwakeSystem<UIConnectLoadingComponent>
    {
        public override void Awake(UIConnectLoadingComponent self)
        {
            self.Awake();
        }
    }
    /// <summary>
    /// 告警弹出弹窗组件
    /// </summary>
    public class UIConnectLoadingComponent : Component
    {

        public void Awake()
        {
            ReferenceCollector rc = this.GetParent<UI>().GameObject.GetComponent<ReferenceCollector>();
        }
    }
}
