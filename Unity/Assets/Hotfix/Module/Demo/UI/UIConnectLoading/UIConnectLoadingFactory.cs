﻿using System;
using ETModel;
using UnityEngine;

namespace ETHotfix
{
    public static class UIConnectLoadingFactory
    {
        public static UI Create()
        {
            try
            {
                ResourcesComponent resourcesComponent = ETModel.Game.Scene.GetComponent<ResourcesComponent>();
                resourcesComponent.LoadBundle(UIType.UIConnectLoading.StringToAB());
                GameObject bundleGameObject = (GameObject)resourcesComponent.GetAsset(UIType.UIConnectLoading.StringToAB(), UIType.UIConnectLoading);
                GameObject gameObject = UnityEngine.Object.Instantiate(bundleGameObject);

                UI ui = ComponentFactory.Create<UI, string, GameObject>(UIType.UIConnectLoading, gameObject, false);

                ui.AddComponent<UIConnectLoadingComponent>();
                return ui;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
        }
    }
}