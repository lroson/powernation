﻿using ETModel;

namespace ETHotfix
{
    [Event(EventIdType.RemoveConnectLoadingUI)]
    public class RemoveConnectLoadingUI : AEvent
    {
        public override void Run()
        {
            Game.Scene.GetComponent<UIComponent>().Remove(UIType.UIConnectLoading);
            ETModel.Game.Scene.GetComponent<ResourcesComponent>().UnloadBundle(UIType.UIConnectLoading.StringToAB());
            //if (ETModel.Game.Scene.GetComponent<ResourcesComponent>().HasBundle(UIType.UIConnectLoading.StringToAB()))
            //{
                
            //}
        }
    }
}
