﻿using System;
using System.Threading;
using ETModel;
using UnityEngine;
using UnityEngine.UI;

namespace ETHotfix
{
    [ObjectSystem]
    public class UIWarningComponentAwakeSystem : AwakeSystem<UIWarningComponent, string, ETTaskCompletionSource, bool>
    {
        public override void Awake(UIWarningComponent self, string result, ETTaskCompletionSource tcs, bool confirmBtnInteractable)
        {
            self.Awake(result, tcs, confirmBtnInteractable).Coroutine();
        }
    }
    [ObjectSystem]
    public class UIWarningComponentAwakeSystem1 : AwakeSystem<UIWarningComponent, string, Action<bool>>
    {
        public override void Awake(UIWarningComponent self, string result, Action<bool> action)
        {
            self.Awake(result, action);
        }
    }
    [ObjectSystem]
    public class UIWarningComponentUpdateSystem : UpdateSystem<UIWarningComponent>
    {
        public override void Update(UIWarningComponent self)
        {
            if (self.isDone && self.tcs != null)
            {
                self.tcs.SetResult();
                Game.EventSystem.Run(EventIdType.RemoveWarningUI3);
            }
        }
    }
    /// <summary>
    /// 告警弹出弹窗组件
    /// </summary>
    public class UIWarningComponent : Component
    {
        public ETTaskCompletionSource tcs;
        public bool isDone;
        private GameObject content;
        private GameObject confirmBtn;
        private GameObject yesBtn;
        private GameObject noBtn;
        private Action<bool> action;
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        public async ETVoid Awake(string info, ETTaskCompletionSource t, bool confirmButtonInteractable)
        {
            this.tcs = t;
            ReferenceCollector rc = this.GetParent<UI>().GameObject.GetComponent<ReferenceCollector>();
            content = rc.Get<GameObject>("Content");
            this.content.GetComponent<Text>().text = info;
            confirmBtn = rc.Get<GameObject>("ConfirmBtn");
            confirmBtn.SetActive(true);
            cancellationTokenSource = new CancellationTokenSource();
            this.confirmBtn.GetComponent<Button>().interactable = confirmButtonInteractable;
            this.confirmBtn.GetComponent<Button>().onClick.Add(this.RemoveWarning);
            await ETModel.Game.Scene.GetComponent<TimerComponent>().WaitAsync(5000, this.cancellationTokenSource.Token);
            this.RemoveWarning();
        }
        public void Awake(string info, Action<bool> result)
        {
            this.action = result;
            ReferenceCollector rc = this.GetParent<UI>().GameObject.GetComponent<ReferenceCollector>();
            content = rc.Get<GameObject>("Content");
            this.content.GetComponent<Text>().text = info;
            yesBtn = rc.Get<GameObject>("YesBtn");
            this.yesBtn.SetActive(true);
            yesBtn.GetComponent<Button>().Add(YesBtnEvent);
            noBtn = rc.Get<GameObject>("NoBtn");
            this.noBtn.SetActive(true);
            noBtn.GetComponent<Button>().Add(NoBtnEvent);
        }

        private void NoBtnEvent()
        {
            action?.Invoke(false);
            Game.EventSystem.Run(EventIdType.RemoveWarningUI2);
        }

        private void YesBtnEvent()
        {
            action?.Invoke(true);
            Game.EventSystem.Run(EventIdType.RemoveWarningUI2);
        }

        void RemoveWarning()
        {
            this.isDone = true;
            this.cancellationTokenSource?.Cancel();
        }
    }
}
