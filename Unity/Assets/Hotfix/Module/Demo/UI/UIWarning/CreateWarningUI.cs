﻿using System;
using ETModel;

namespace ETHotfix
{
    [Event(EventIdType.CreateWarningUI3)]
    public class CreateWarningUI : AEvent<string, ETTaskCompletionSource, bool>
    {
        public override void Run(string content, ETTaskCompletionSource tcs, bool confirmBtnInteractable)
        {
            UI ui = UIWarningFactory.Create(content, tcs, confirmBtnInteractable);
            Game.Scene.GetComponent<UIComponent>().Add(ui);
        }
    }

    [Event(EventIdType.CreateWarningUI2)]
    public class CreateWarningUIWithAction : AEvent<string, Action<bool>>
    {
        public override void Run(string content, Action<bool> action)
        {
            UI ui = UIWarningFactory.Create(content, action);
            Game.Scene.GetComponent<UIComponent>().Add(ui);
        }
    }
}
