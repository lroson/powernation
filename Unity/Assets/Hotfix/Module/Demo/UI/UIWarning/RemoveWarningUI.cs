﻿using ETModel;

namespace ETHotfix
{
    [Event(EventIdType.RemoveWarningUI2)]
    public class RemoveWarningUI2 : AEvent
    {
        public override void Run()
        {
            Game.Scene.GetComponent<UIComponent>().Remove(UIType.UIWarning);
            ETModel.Game.Scene.GetComponent<ResourcesComponent>().UnloadBundle(UIType.UIWarning.StringToAB());
            //if (ETModel.Game.Scene.GetComponent<ResourcesComponent>().HasBundle(UIType.UIWarning.StringToAB()))
            //{
                
            //}
        }
    }
    [Event(EventIdType.RemoveWarningUI3)]
    public class RemoveWarningUI3 : AEvent
    {
        public override void Run()
        {
            Game.Scene.GetComponent<UIComponent>().Remove(UIType.UIWarning);
            ETModel.Game.Scene.GetComponent<ResourcesComponent>().UnloadBundle(UIType.UIWarning.StringToAB());
            //if (ETModel.Game.Scene.GetComponent<ResourcesComponent>().HasBundle(UIType.UIWarning.StringToAB()))
            //{

            //}
        }
    }
}
