﻿using System;
using ETModel;
using UnityEngine;

namespace ETHotfix
{
    public static class UIWarningFactory
    {
        public static UI Create(string content, ETTaskCompletionSource tcs, bool confirmBtnInteractable)
        {
            try
            {
                ResourcesComponent resourcesComponent = ETModel.Game.Scene.GetComponent<ResourcesComponent>();
                resourcesComponent.LoadBundle(UIType.UIWarning.StringToAB());
                GameObject bundleGameObject = (GameObject)resourcesComponent.GetAsset(UIType.UIWarning.StringToAB(), UIType.UIWarning);
                GameObject gameObject = UnityEngine.Object.Instantiate(bundleGameObject);

                UI ui = ComponentFactory.Create<UI, string, GameObject>(UIType.UIWarning, gameObject, false);

                ui.AddComponent<UIWarningComponent, string, ETTaskCompletionSource, bool>(content, tcs, confirmBtnInteractable);
                return ui;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
        }
        public static UI Create(string content, Action<bool> action)
        {
            try
            {
                ResourcesComponent resourcesComponent = ETModel.Game.Scene.GetComponent<ResourcesComponent>();
                resourcesComponent.LoadBundle(UIType.UIWarning.StringToAB());
                GameObject bundleGameObject = (GameObject)resourcesComponent.GetAsset(UIType.UIWarning.StringToAB(), UIType.UIWarning);
                GameObject gameObject = UnityEngine.Object.Instantiate(bundleGameObject);

                UI ui = ComponentFactory.Create<UI, string, GameObject>(UIType.UIWarning, gameObject, false);

                ui.AddComponent<UIWarningComponent, string, Action<bool>>(content, action);
                return ui;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
        }
    }
}