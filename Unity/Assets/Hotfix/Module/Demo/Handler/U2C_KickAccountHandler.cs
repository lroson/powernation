﻿using ETModel;

namespace ETHotfix
{
    [MessageHandler]
    public class U2C_KickAccountHandler : AMHandler<U2C_KickAccount>
    {
        protected override async ETTask Run(ETModel.Session session, U2C_KickAccount message)
        {
            Game.Scene.GetComponent<GameAisleComponent>().StartGame(GameAisleId.Login);
            await WarningHelper.WarningAsync("您的账号在别处登陆", true);
        }
    }
}
