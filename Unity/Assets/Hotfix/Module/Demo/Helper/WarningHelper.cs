using System;
using ETModel;

namespace ETHotfix
{
    public static class WarningHelper
    {
        /// <summary>
        /// 异步弹出告警提示
        /// </summary>
        /// <param name="Info">提示信息</param>
        /// <param name="isIinteractable">确认按钮是否可以交互</param>
        /// <returns></returns>
        public static ETTask WarningAsync(string Info, bool isIinteractable)
        {
            ETTaskCompletionSource tcs = new ETTaskCompletionSource();
            Game.EventSystem.Run(EventIdType.CreateWarningUI3, Info, tcs, isIinteractable);
            return tcs.Task;
        }

        public static void WarningWithAction(string Info, Action<bool> action)
        {
            Game.EventSystem.Run(EventIdType.CreateWarningUI2, Info, action);
        }
    }
}