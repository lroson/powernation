//using System;
//using ETModel;
//using UnityEngine;
//using UnityEngine.SceneManagement;

//namespace ETHotfix
//{
//    public static class LoginHelper
//    {
//        public static async ETTask OnLoginAsync(string account, string password, bool isLogin)
//        {
//            try
//            {
//                创建一个ETModel层的Session
//                ETModel.Session session = ETModel.Game.Scene.GetComponent<NetOuterComponent>().Create(GlobalConfigComponent.Instance.GlobalProto.Address);

//                创建一个ETHotfix层的Session, ETHotfix的Session会通过ETModel层的Session发送消息
//               Session realmSession = ComponentFactory.Create<Session, ETModel.Session>(session);
//                R2C_Login r2CLogin = (R2C_Login)await realmSession.Call(new C2R_Login() { Account = account, Password = password, IsLogin = isLogin });

//                realmSession.Dispose();

//                if (r2CLogin.Address == null || r2CLogin.Key == 0)
//                {
//                    switch (r2CLogin.Error)
//                    {
//                        case ErrorCode.ERR_AccountPasswordEmpty:
//                            await WarningHelper.WarningPopAsync("<color=red>账号或密码为空</color>，请重新输入！", true);
//                            break;
//                        case ErrorCode.ERR_LoginAccountOrPasswordNoMatch:
//                            await WarningHelper.WarningPopAsync("登录<color=red>账号密码错误</color>，请重新输入！", true);
//                            break;
//                        case ErrorCode.ERR_LoginAccountOverload:
//                            await WarningHelper.WarningPopAsync("登录<color=red>服务器人数已满</color>，请稍后登录！", true);
//                            break;
//                        case ErrorCode.ERR_LoginAccountOnline:
//                            await WarningHelper.WarningPopAsync("登录<color=red>账号在线</color>，请重新登录！", true);
//                            break;
//                        case ErrorCode.ERR_RegisterAccountExist:
//                            await WarningHelper.WarningPopAsync("注册<color=red>账号已经存在</color>，请重新输入！", true);
//                            break;
//                        case ErrorCode.ERR_RegisterSuccess:
//                            await WarningHelper.WarningPopAsync("注册<color=green>账号成功</color>，请登录进入游戏，祝君愉快！", true);
//                            break;
//                    }
//                    return;
//                }


//                // 创建一个ETModel层的Session,并且保存到ETModel.SessionComponent中
//                ETModel.Session gateSession = ETModel.Game.Scene.GetComponent<NetOuterComponent>().Create(r2CLogin.Address);
//                ETModel.Game.Scene.AddComponent<ETModel.SessionComponent>().Session = gateSession;

//                // 创建一个ETHotfix层的Session, 并且保存到ETHotfix.SessionComponent中
//                Game.Scene.AddComponent<SessionComponent>().Session = ComponentFactory.Create<Session, ETModel.Session>(gateSession);

//                G2C_LoginGate g2CLoginGate = (G2C_LoginGate)await SessionComponent.Instance.Session.Call(new C2G_LoginGate() { Account = account, Key = r2CLogin.Key });
//                if (g2CLoginGate.Error == ErrorCode.ERR_ConnectGateKeyError)
//                {
//                    await WarningHelper.WarningPopAsync($"<color=green>{r2CLogin.Message}</color>", false);
//                    return;
//                }
//                Log.Info("登陆gate成功!");
//                SessionComponent.Instance.Session.AddComponent<HeartBeatComponent>();
//                // 创建Player
//                Player player = ETModel.ComponentFactory.CreateWithId<Player>(g2CLoginGate.PlayerId);
//                PlayerComponent playerComponent = ETModel.Game.Scene.GetComponent<PlayerComponent>();
//                playerComponent.MyPlayer = player;
//                //注册session断线重连
//                ETModel.SessionComponent.Instance.Session.GetComponent<SessionCallbackComponent>().DisposeCallback += async s =>
//                {
//                    await WarningHelper.WarningPopAsync("<color=red>服务器连接中断，正在重新连接。。。</color>", false);
//                    await OnLoginAsync(account, password, isLogin);

//                };
//                Game.EventSystem.Run(EventIdType.LoginFinish);

//                // 测试消息有成员是class类型
//                G2C_PlayerInfo g2CPlayerInfo = (G2C_PlayerInfo)await SessionComponent.Instance.Session.Call(new C2G_PlayerInfo());
//            }
//            catch (Exception e)
//            {
//                await WarningHelper.WarningPopAsync("<color=red>网络连接错误或服务器正在维护</color>，请稍后再试！", false);
//                Log.Error(e);
//                await ReturnLogin();
//            }
//        }


//        static async ETTask ReturnLogin()
//        {
//            if (SceneManager.GetActiveScene().buildIndex != 0)
//            {
//                ETModel.Game.Scene.GetComponent<ResourcesComponent>().UnloadAll();
//                try
//                {
//                    using (SceneChangeComponent sceneChangeComponent = ETModel.Game.Scene.AddComponent<SceneChangeComponent>())
//                    {
//                        await sceneChangeComponent.ChangeSceneAsync(SceneType.Init);
//                    }
//                    Game.EventSystem.Run(EventIdType.InitSceneStart);
//                }
//                catch (Exception e)
//                {
//                    Log.Error(e);
//                }
//            }
//            else
//            {
//                if (!ETModel.Game.Scene.GetComponent<ResourcesComponent>().HasBundle(UIType.UILogin.StringToAB()))
//                {
//                    Game.Scene.GetComponent<UIComponent>().RemoveAll();
//                    ETModel.Game.Scene.GetComponent<ResourcesComponent>().UnloadAll();
//                    Game.EventSystem.Run(EventIdType.InitSceneStart);
//                }
//            }
//        }
//    }
//}