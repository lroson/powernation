﻿namespace ETHotfix
{
    public class CopyUser
    {
        public static User Copy(User user)
        {
            User newUser = new User();
            newUser.UserId = user.UserId;
            newUser.Coins = user.Coins;
            newUser.Gold = user.Gold;
            newUser.Name = user.Name;
            newUser.Icon = user.Icon;
            newUser.Sex = user.Sex;
            newUser.Ip = user.Ip;
            newUser.Location = user.Location;
            newUser.IsOnLine = user.IsOnLine;
            return newUser;
        }
    }
}
