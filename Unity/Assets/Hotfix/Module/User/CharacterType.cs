﻿namespace ETHotfix
{
    /// <summary>
    /// 角色类型，1-5为自定义类型，1000以后为历史武将
    /// </summary>
    public class CharacterType
    {
        public const int None = 0;//不确定
        public const int General = 1;//将军型（统帅）
        public const int Warrior = 2;//勇将型（武力）
        public const int Stategist = 3;//军师型（智力）
        public const int Politicion = 4;//内政型（政治）
        public const int Allround = 5;//全能型（均衡）
    }
    /// <summary>
    /// 将军型（统帅）
    /// </summary>
    public class GeneralChar
    {
        public const int CommanderBase = 80;
        public const int ForceBase = 60;
        public const int IntelligenceBase = 40;
        public const int PoliticsBase = 40;
        
        public const int CommanderGrow = 4;
        public const int ForceGrow = 2;
        public const int IntelligenceGrow = 1;
        public const int PoliticsGrow = 1;
    }
    /// <summary>
    /// 勇将型（武力）
    /// </summary>
    public class WarriorChar
    {
        public const int CommanderBase = 60;
        public const int ForceBase = 80;
        public const int IntelligenceBase = 40;
        public const int PoliticsBase = 40;

        public const int CommanderGrow = 2;
        public const int ForceGrow = 4;
        public const int IntelligenceGrow = 1;
        public const int PoliticsGrow = 1;
    }
    /// <summary>
    /// 军师型（智力）
    /// </summary>
    public class IntelligenceChar
    {
        public const int CommanderBase = 40;
        public const int ForceBase = 40;
        public const int IntelligenceBase = 80;
        public const int PoliticsBase = 60;

        public const int CommanderGrow = 1;
        public const int ForceGrow = 1;
        public const int IntelligenceGrow = 4;
        public const int PoliticsGrow = 2;
    }

    /// <summary>
    /// 内政型（政治）
    /// </summary>
    public class PoliticianChar
    {
        public const int CommanderBase = 40;
        public const int ForceBase = 40;
        public const int IntelligenceBase = 60;
        public const int PoliticsBase = 80;

        public const int CommanderGrow = 1;
        public const int ForceGrow = 1;
        public const int IntelligenceGrow = 2;
        public const int PoliticsGrow = 4;
    }
    /// <summary>
    /// 全能型（均衡）
    /// </summary>
    public class AllroundChar
    {
        public const int CommanderBase = 55;
        public const int ForceBase = 55;
        public const int IntelligenceBase = 55;
        public const int PoliticsBase = 55;

        public const int CommanderGrow = 2;
        public const int ForceGrow = 2;
        public const int IntelligenceGrow = 2;
        public const int PoliticsGrow = 2;
    }
}
