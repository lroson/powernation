﻿using ETModel;

namespace ETHotfix
{
    public class GoodsId
    {
        public const long None = 0;
        public const long CNY = 99;
        public const long Gold = 1000;
        public const long Coins = 1001;
        public const long HongBao = 1002;
    }

    public static class GoodsInfoTool
    {
        public static string GetGoodsIcon(long goodsId)
        {
            switch (goodsId)
            {
                case GoodsId.Gold:
                    return "GoldIcon";
                case GoodsId.Coins:
                    return "CoinsIcon";
                case GoodsId.HongBao:
                    return "HongBaoIcon";
            }
            Log.Error($"物品:{goodsId}没有对应的图标");
            return "";
        }

        public static string GetGoodsName(long goodsId)
        {
            switch (goodsId)
            {
                case GoodsId.Gold:
                    return "元宝";
                case GoodsId.Coins:
                    return "铜钱";
                case GoodsId.HongBao:
                    return "红包";
            }
            Log.Error($"物品:{goodsId}没有对应的名字");
            return "";
        }
    }
}
