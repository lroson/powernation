using ETModel;
namespace ETHotfix
{
//去验证验证账号 请求秘钥
	[Message(HotfixOpcode.C2R_CommonLogin)]
	public partial class C2R_CommonLogin : IRequest {}

	[Message(HotfixOpcode.R2C_CommonLogin)]
	public partial class R2C_CommonLogin : IResponse {}

//拿着秘钥去登陆
	[Message(HotfixOpcode.C2G_GateLogin)]
	public partial class C2G_GateLogin : IRequest {}

	[Message(HotfixOpcode.G2C_GateLogin)]
	public partial class G2C_GateLogin : IResponse {}

	[Message(HotfixOpcode.C2G_Heartbeat)]
	public partial class C2G_Heartbeat : IMessage {}

	[Message(HotfixOpcode.User)]
	public partial class User {}

	[Message(HotfixOpcode.Character)]
	public partial class Character {}

//通知客户端被挤号了
	[Message(HotfixOpcode.U2C_KickAccount)]
	public partial class U2C_KickAccount : IActorMessage {}

	[Message(HotfixOpcode.G2C_TestHotfixMessage)]
	public partial class G2C_TestHotfixMessage : IMessage {}

	[Message(HotfixOpcode.C2M_TestActorRequest)]
	public partial class C2M_TestActorRequest : IActorLocationRequest {}

	[Message(HotfixOpcode.M2C_TestActorResponse)]
	public partial class M2C_TestActorResponse : IActorLocationResponse {}

//查询User信息
	[Message(HotfixOpcode.C2L_GetUserInfo)]
	public partial class C2L_GetUserInfo : IRequest {}

	[Message(HotfixOpcode.L2C_GetUserInfo)]
	public partial class L2C_GetUserInfo : IResponse {}

	[Message(HotfixOpcode.Commodity)]
	public partial class Commodity {}

	[Message(HotfixOpcode.SignInAward)]
	public partial class SignInAward {}

	[Message(HotfixOpcode.UserSingInState)]
	public partial class UserSingInState {}

	[Message(HotfixOpcode.GetGoodsOne)]
	public partial class GetGoodsOne {}

//通知玩家得到了物品
	[Message(HotfixOpcode.Actor_UserGetGoods)]
	public partial class Actor_UserGetGoods : IActorMessage {}

//房间信息
	[Message(HotfixOpcode.RoomInfo)]
	public partial class RoomInfo {}

//游戏匹配房间配置
	[Message(HotfixOpcode.MatchRoomConfig)]
	public partial class MatchRoomConfig {}

//匹配到的玩家信息
	[Message(HotfixOpcode.MatchPlayerInfo)]
	public partial class MatchPlayerInfo {}

//设置封号 和解封
	[Message(HotfixOpcode.C2U_SetIsStopSeal)]
	public partial class C2U_SetIsStopSeal : IAdministratorRequest {}

	[Message(HotfixOpcode.U2C_SetIsStopSeal)]
	public partial class U2C_SetIsStopSeal : IResponse {}

//更改用户 物品
	[Message(HotfixOpcode.C2U_ChangeUserGoods)]
	public partial class C2U_ChangeUserGoods : IAdministratorRequest {}

	[Message(HotfixOpcode.U2C_ChangeUserGoods)]
	public partial class U2C_ChangeUserGoods : IResponse {}

//获取在线人数
	[Message(HotfixOpcode.C2U_GetOnLineNumber)]
	public partial class C2U_GetOnLineNumber : IAdministratorRequest {}

	[Message(HotfixOpcode.U2C_GetOnLineNumber)]
	public partial class U2C_GetOnLineNumber : IResponse {}

//获取用户交易记录
	[Message(HotfixOpcode.C2U_GetGoodsDealRecord)]
	public partial class C2U_GetGoodsDealRecord : IAdministratorRequest {}

	[Message(HotfixOpcode.U2C_GetGoodsDealRecord)]
	public partial class U2C_GetGoodsDealRecord : IResponse {}

//查询用户信息
	[Message(HotfixOpcode.C2U_QueryUserInfo)]
	public partial class C2U_QueryUserInfo : IAdministratorRequest {}

	[Message(HotfixOpcode.U2C_QueryUserInfo)]
	public partial class U2C_QueryUserInfo : IResponse {}

//查询封号记录
	[Message(HotfixOpcode.C2U_QueryStopSealRecord)]
	public partial class C2U_QueryStopSealRecord : IAdministratorRequest {}

	[Message(HotfixOpcode.U2C_QueryStopSealRecord)]
	public partial class U2C_QueryStopSealRecord : IResponse {}

//单次封号信息
	[Message(HotfixOpcode.StopSealRecord)]
	public partial class StopSealRecord {}

//单次用户钻石交易记录
	[Message(HotfixOpcode.GoodsDealRecord)]
	public partial class GoodsDealRecord {}

//发送给客户端 告诉客户端该用户是否正在游戏中
	[Message(HotfixOpcode.Mh2C_BeingInGame)]
	public partial class Mh2C_BeingInGame : IActorMessage {}

}
namespace ETHotfix
{
	public static partial class HotfixOpcode
	{
		 public const ushort C2R_CommonLogin = 10001;
		 public const ushort R2C_CommonLogin = 10002;
		 public const ushort C2G_GateLogin = 10003;
		 public const ushort G2C_GateLogin = 10004;
		 public const ushort C2G_Heartbeat = 10005;
		 public const ushort User = 10006;
		 public const ushort Character = 10007;
		 public const ushort U2C_KickAccount = 10008;
		 public const ushort G2C_TestHotfixMessage = 10009;
		 public const ushort C2M_TestActorRequest = 10010;
		 public const ushort M2C_TestActorResponse = 10011;
		 public const ushort C2L_GetUserInfo = 10012;
		 public const ushort L2C_GetUserInfo = 10013;
		 public const ushort Commodity = 10014;
		 public const ushort SignInAward = 10015;
		 public const ushort UserSingInState = 10016;
		 public const ushort GetGoodsOne = 10017;
		 public const ushort Actor_UserGetGoods = 10018;
		 public const ushort RoomInfo = 10019;
		 public const ushort MatchRoomConfig = 10020;
		 public const ushort MatchPlayerInfo = 10021;
		 public const ushort C2U_SetIsStopSeal = 10022;
		 public const ushort U2C_SetIsStopSeal = 10023;
		 public const ushort C2U_ChangeUserGoods = 10024;
		 public const ushort U2C_ChangeUserGoods = 10025;
		 public const ushort C2U_GetOnLineNumber = 10026;
		 public const ushort U2C_GetOnLineNumber = 10027;
		 public const ushort C2U_GetGoodsDealRecord = 10028;
		 public const ushort U2C_GetGoodsDealRecord = 10029;
		 public const ushort C2U_QueryUserInfo = 10030;
		 public const ushort U2C_QueryUserInfo = 10031;
		 public const ushort C2U_QueryStopSealRecord = 10032;
		 public const ushort U2C_QueryStopSealRecord = 10033;
		 public const ushort StopSealRecord = 10034;
		 public const ushort GoodsDealRecord = 10035;
		 public const ushort Mh2C_BeingInGame = 10036;
	}
}
