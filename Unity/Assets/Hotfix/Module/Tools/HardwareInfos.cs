﻿namespace ETHotfix
{
    public static class PlatformType
    {
        public const int Android = 1;
        public const int IOS = 2;
        public const int PC = 3;
    }

    public static class HardwareInfos
    {
#if UNITY_ANDROID
         public static int CurrentPlatform = PlatformType.Android;
#endif

#if UNITY_IPHONE
         public static int CurrentPlatform = PlatformType.IOS;
#endif

#if UNITY_STANDALONE_WIN
        public static int CurrentPlatform = PlatformType.PC;
#endif
        

        
    }
}

