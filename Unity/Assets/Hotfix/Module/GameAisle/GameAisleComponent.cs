﻿using ETModel;
using System;
using System.Collections.Generic;

namespace ETHotfix
{
    [ObjectSystem]
    public class ToyGameComponentAwakeSystem : AwakeSystem<GameAisleComponent>
    {
        public override void Awake(GameAisleComponent self)
        {
            self.Awake();
        }
    }
    public class GameAisleComponent : Component
    {
        public long CurrToyGame = GameAisleId.None;
        private readonly Dictionary<long, GameAisleBase> gameAisleBaseDic = new Dictionary<long, GameAisleBase>();
        public void Awake()
        {
            gameAisleBaseDic.Clear();
            List<Type> types = Game.EventSystem.GetTypes();

            foreach (Type type in types)
            {
                object[] attrs = type.GetCustomAttributes(typeof(GameAisleAttribute), false);

                if (attrs.Length == 0)
                {
                    continue;
                }
                GameAisleAttribute toyGameAttribute = attrs[0] as GameAisleAttribute;
                GameAisleBase toyGameAisleBase = Activator.CreateInstance(type) as GameAisleBase;
                toyGameAisleBase.Awake(toyGameAttribute.Type);
                gameAisleBaseDic.Add(toyGameAttribute.Type, toyGameAisleBase);
            }
        }

        public void StartGame(long gameType, params object[] objs)
        {
            if (gameAisleBaseDic.ContainsKey(gameType))
            {
                //当前游戏通道不为0，并且即将进入的游戏通道不等于当前通道，先结束当前通道，再进入新通道
                if (CurrToyGame != GameAisleId.None && this.CurrToyGame != gameType)
                {
                    gameAisleBaseDic[CurrToyGame].EndGame();
                }
                gameAisleBaseDic[gameType].StartGame(objs);
            }
            else
            {
                Log.Error("想要进入的游戏不存在:" + gameType);
            }
        }
        public void EndGame()
        {
            if (gameAisleBaseDic.ContainsKey(CurrToyGame))
            {
                gameAisleBaseDic[CurrToyGame].EndGame();
            }
            else
            {
                Log.Error("系统错误,目前状态游戏不存在:" + CurrToyGame);
            }
        }
    }
}