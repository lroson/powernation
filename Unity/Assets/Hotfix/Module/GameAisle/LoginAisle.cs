﻿using ETModel;

namespace ETHotfix
{
    [GameAisle(GameAisleId.Login)]
    public class LoginAisle : GameAisleBase
    {
        public override void StartGame(params object[] objs)
        {
            base.StartGame();
            Log.Debug("进入登陆界面");
            //EventMsgMgr.AllRemoveEvent();
            Game.Scene.GetComponent<KCPUseManage>().InitiativeDisconnect();
            //Game.Scene.GetComponent<UIComponent>().RemoveAll();
            Game.EventSystem.Run(EventIdType.OpenInitScene);
            //Game.Scene.GetComponent<UIComponent>().Show(UIType.LoginPanel);
        }

        public override void EndGame()
        {
            Game.EventSystem.Run(EventIdType.CloseInitScene);
            // Log.Error("处于登陆界面无法退出");
            //Game.Scene.GetComponent<UIComponent>().Show(UIType.LobbyPanel);
        }
    }
}
