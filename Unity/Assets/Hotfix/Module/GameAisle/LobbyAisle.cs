﻿using ETModel;

namespace ETHotfix
{
    [GameAisle(GameAisleId.Lobby)]
    public class LobbyAisle : GameAisleBase
    {
        public override void StartGame(params object[] objs)
        {
            base.StartGame();
            Log.Debug("进入大厅");
            Game.EventSystem.Run(EventIdType.CreateLobbyUI);
        }

        public override void EndGame()
        {
            Game.EventSystem.Run(EventIdType.RemoveLobbyUI);
        }
    }
}
