﻿namespace ETHotfix
{
    public abstract class GameAisleBase
    {
        public long ToyGameType { private set; get; }

        private GameAisleComponent toyGameComponent;
        protected GameAisleComponent ToyGameComponent
        {
            get
            {
                if (toyGameComponent == null)
                {
                    toyGameComponent=Game.Scene.GetComponent<GameAisleComponent>();
                }
                return toyGameComponent;
            }
        }
        public virtual void Awake(long gameType)
        {
            ToyGameType = gameType;
        }

        public virtual void StartGame(params object[] objs)
        {
            ToyGameComponent.CurrToyGame = ToyGameType;
        }
        
        //不一定调   玩家调用直接开启其他游戏就不会调 后调
        public virtual void EndGame()
        {
            
        }

       
    }
}
