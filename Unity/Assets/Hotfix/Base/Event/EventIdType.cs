﻿namespace ETHotfix
{
	public static class EventIdType
	{
		public const string OpenInitScene = "OpenInitScene";
		public const string CloseInitScene = "CloseInitScene";
	    public const string CreateLobbyUI = "CreateLobbyUI";
	    public const string RemoveLobbyUI = "LobbyFinish";//TODO MapHel调整perd中需要
        public const string EnterMap = "EnterMap";
        public const string MapFinish = "MapFinish";
	    public const string CreateWarningUI2 = "CreateWarningUI2";//2参数
	    public const string RemoveWarningUI2 = "RemoveWarningUI2";
	    public const string CreateWarningUI3 = "CreateWarningUI3";//3参数
	    public const string RemoveWarningUI3 = "RemoveWarningUI3";
        public const string CreateConnectLoadingUI = "CreateConnectLoadingUI";
	    public const string RemoveConnectLoadingUI = "RemoveConnectLoadingUI";

    }
}